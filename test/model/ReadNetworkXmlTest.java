/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import model.ReadNetworkXml;
import model.Junction;
import model.Project;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.xml.sax.SAXException;
import sun.swing.SwingUtilities2;
import sun.swing.SwingUtilities2.Section;

/**
 *
 * @author 1140921
 */
public class ReadNetworkXmlTest {

    public ReadNetworkXmlTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() throws ParserConfigurationException, SAXException {
        Project project = new Project("", "2");
        String id = "TestSet02";
        String description = "5 node test set";
        Junction node1 = new Junction("n0");
        Junction node2 = new Junction("n1");
        Junction node3 = new Junction("n2");

        Segment s01 = new Segment("01", 100, 1.5f, 3200, 20);
        InputData road = new ReadNetworkXml();
        road.read(project, "TestSet02_Network.xml");
//        assertTrue("should be TestSet02", project.getName().equalsIgnoreCase(id));
//        assertTrue("should be 5 node test set", project.getDescription().equalsIgnoreCase(description));
        assertTrue("should add 5", project.getRoadNetwork().numVertices() == 5);
        System.out.println("-->" + project.getRoadNetwork().toString());
        Vertex v = project.getRoadNetwork().getVertex(0);
        Vertex v1 = project.getRoadNetwork().getVertex(1);
        Vertex v2 = project.getRoadNetwork().getVertex(2);

        Edge s = project.getRoadNetwork().getEdge(v, v1);
        Edge s1 = project.getRoadNetwork().getEdge(v1, v);
        Edge s2 = project.getRoadNetwork().getEdge(v1, v2);
        Edge s3 = project.getRoadNetwork().getEdge(v2, v1);
        Edge s4 = project.getRoadNetwork().getEdge(v, v2);
        Edge s5 = project.getRoadNetwork().getEdge(v2, v);
        Edge s6 = project.getRoadNetwork().getEdge(v1, v2);
        Edge s7 = project.getRoadNetwork().getEdge(v2, v1);

//        System.out.println("s : vOrig = n0 vDest = n1");
//        assertTrue("Should be n0", s.getVOrig().equals(s1.getVDest()));
//
//        System.out.println("s1 : vOrig = n1 vDest = n0");
//        assertTrue("Should be n1", s1.getVOrig().equals(s1.getVOrig()));
//
//        System.out.println("s2 : vOrig = n1 vDest = n2");
//        assertTrue("Should be n1", s2.getVOrig().equals(s3.getVDest()));
//
//        System.out.println("s3 : vOrig = n2 vDest = n1");
//        assertTrue("Should be n2", s3.getVOrig().equals(s2.getVDest()));
//
//        System.out.println("s4 : vOrig = n0 vDest = n2");
//        assertTrue("Should be n0", s4.getVOrig().equals(s5.getVDest()));
//
//        System.out.println("s5 : vOrig = n2 vDest = n0");
//        assertTrue("Should be n2", s5.getVOrig().equals(s4.getVDest()));
//
//        System.out.println("s6 : vOrig = n1 vDest = n2");
//        assertTrue("Should be n1", s6.getVOrig().equals(s7.getVDest()));
        Iterable<Edge<Junction, List<RoadSection>>> edges = project.getRoadNetwork().edges();
        System.out.println("Road Id :");
        int i = 0;
        System.out.println("" + project.getRoadNetwork());

        for (Edge<Junction, List<RoadSection>> r : edges) {
            System.out.println("-->" + r.getElement().get(0).getRoadId());
            System.out.println("-->" + r.getVOrig());
            System.out.println("-->" + r.getVDest());
            if (r.getElement().size() > 1) {
                System.out.println("----");

                for (RoadSection roadS : r.getElement()) {
                    System.out.println("--|" + roadS.getRoadId());
                }
                System.out.println("----");
            }

        }

        project.getRoadNetwork().getEdge(v2, v1);
        assertTrue("should add 12", project.getRoadNetwork().numEdges() == 12);

    }

    @Test
    public void testDuplicate() throws ParserConfigurationException, SAXException {
        Project project = new Project("Teste02", "2");

        InputData road = new ReadNetworkXml();
        road.read(project, "TestSet01_Network.xml");
                assertTrue("should be 3"+project.getRoadNetwork().numVertices(), project.getRoadNetwork().numVertices()==3);

//        for (Edge<Junction, List<RoadSection>> r : project.getRoadNetwork().edges()) {
//            for (RoadSection roads : r.getElement()) {
//                System.out.println("" + roads.getBeginningNode());
//            }
        road.read(project, "TestSet01_Network(1).xml");
        assertTrue("should be 5  "+project.getRoadNetwork().numVertices(), project.getRoadNetwork().numVertices()==5);
        assertTrue("should be 10  "+project.getRoadNetwork().numEdges(), project.getRoadNetwork().numEdges()==10);
        for (Edge<Junction, List<RoadSection>> r : project.getRoadNetwork().edges()) {
            for (RoadSection roads : r.getElement()) {
                System.out.println("" + roads.getBeginningNode());
            }
        }

    }

}
