/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140921
 */
public class FastestPathTest {

    public FastestPathTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getPath method, of class FastestPath.
     */
    @Test
    public void testGetPath() {
        Project project = new Project("Teste", "S01");

        System.out.println("getPath");
        ReadNetworkXml network = new ReadNetworkXml();
        network.read(project, "TestSet01_Network.xml");
        ReadVehiclesXML vehicles = new ReadVehiclesXML();
        vehicles.read(project, "TestSet01_Vehicles.xml");
        Graph<Junction, List<RoadSection>> g = project.getRoadNetwork();

        System.out.println("---------------<" + project.getRoadNetwork().vertices().toString());
        Junction orig = g.getVertex(0).getElement();
        Junction dest = g.getVertex(2).getElement();
        Vehicle vehicle = project.getVehicleList().getVehicleList().get(0);
        FastestPath instance = new FastestPath();

        NetworkAnalisysResults result = instance.getPath(g, orig, dest, vehicle);
        System.out.println("--" + Calculus.work(vehicle.getMass(), 100 * 1000 / 3600, 0.01f, 3, -5, 0, 20 * 1000));
        result.getEnergyConsumption();

        System.out.println("--->" + result);
        
        
        
        
        System.out.println("-------------getPath2------------");
         network = new ReadNetworkXml();
        network.read(project, "TestSet01_Network.xml");
         vehicles = new ReadVehiclesXML();
        vehicles.read(project, "TestSet01_Vehicles.xml");
        g = project.getRoadNetwork();

         orig = g.getVertex(2).getElement();
         dest = g.getVertex(0).getElement();
         vehicle = project.getVehicleList().getVehicleList().get(0);
         instance = new FastestPath();

        result = instance.getPath(g, orig, dest, vehicle);
        System.out.println("--" + Calculus.work(vehicle.getMass(), 100 * 1000 / 3600, 0.01f, 3, -5, 0, 20 * 1000));
        result.getEnergyConsumption();

        System.out.println("--->" + result);

    }

    public void testGetPath2() {
            Project project=new Project("Teste", "S01");

        System.out.println("-------------getPath2------------");
        ReadNetworkXml network = new ReadNetworkXml();
        network.read(project, "TestSet01_Network.xml");
        ReadVehiclesXML vehicles = new ReadVehiclesXML();
        vehicles.read(project, "TestSet01_Vehicles.xml");
        Graph<Junction, List<RoadSection>> g = project.getRoadNetwork();

        Junction orig = g.getVertex(2).getElement();
        Junction dest = g.getVertex(0).getElement();
        Vehicle vehicle = project.getVehicleList().getVehicleList().get(0);
        FastestPath instance = new FastestPath();

        NetworkAnalisysResults result = instance.getPath(g, orig, dest, vehicle);
        System.out.println("--" + Calculus.work(vehicle.getMass(), 100 * 1000 / 3600, 0.01f, 3, -5, 0, 20 * 1000));
        result.getEnergyConsumption();

        System.out.println("--->" + result);

    }

}
