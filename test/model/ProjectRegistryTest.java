/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import model.*;
import java.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hichampt
 */
public class ProjectRegistryTest {

    public ProjectRegistryTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of modifiyData method, of class ProjectRegistry.
     */
    @Test
    public void testModifiyData() {
        
        Project p1;
        System.out.println("modifiyData");
        
        String name = "p1";
        String desc = "desc";
        
        p1 = new Project(name, desc);
        ProjectRegistry instance = new ProjectRegistry();
        instance.addProject(p1);
        
        String newName = "newP";
        String newDesc = "newDesc";
        
        instance.setActiveProject(p1);
      
        boolean expResult = true;
        boolean result = instance.modifiyData(newName, newDesc);
        
        assertEquals(expResult, result);
        assertTrue("Name of Active project should be newP ", newName.equals(instance.getActiveProject().getName()) );
    }

 


 

}
