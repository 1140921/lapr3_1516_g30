/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import static model.Calculus.kineticEnergy;
import static model.Calculus.vRelative;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140921
 */
public class CalculusTest {
    
    public CalculusTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of powerGeneratedByEngine method, of class Calculus.
     */
    @Test
    public void testPowerGeneratedByEngine() {
        System.out.println("powerGeneratedByEngine");
        float torque = 85F;
        int rpm = 2482;
        double expResult = 22092.72;
        double result = Calculus.powerGeneratedByEngine(torque, rpm);
        
        assertEquals(expResult, result, 0.2);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

     @Test
    public void testPowerGeneratedByEngineEletric() {
        System.out.println("powerGeneratedByEngine");
        float torque = 85F;
        int rpm = 2482;
        double expResult = 22092.72;
        double result = Calculus.powerGeneratedByEngine(torque, rpm);
        
        assertEquals(expResult, result, 0.2);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
    /**
     * Test of rpm method, of class Calculus.
     */
    @Test
    public void testRpm() {
        System.out.println("Dummy 01");
        System.out.println("rpm");
        float velocity = 27.777778F;
        float finalDriveRatio = 2.6F;
        float gearRatio = 0.9F;
        float radius = 0.25F;
        int expResult = 2482;
        int result = Calculus.rpm(velocity, finalDriveRatio, gearRatio, radius);
        
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    
    @Test
    public void testRpm2() {
        System.out.println("Dummy 02");
        System.out.println("rpm");
        float velocity = 33.333F;
        float finalDriveRatio = 2.6F;
        float gearRatio = 0.7F;
        float radius = 0.25F;
        int expResult = 2317;
        int result = Calculus.rpm(velocity, finalDriveRatio, gearRatio, radius);

        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    @Test
    public void testRpmEletric() {
        System.out.println("Eletric Dummy ");
        System.out.println("rpm");
        float velocity = 25.0F;
        float finalDriveRatio = 2.6F;
        float gearRatio = 1.0F;
        float radius = 0.25F;
        int expResult = 2482;
        int result = Calculus.rpm(velocity, finalDriveRatio, gearRatio, radius);
       
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    
    /**
     * Test of carVelocity method, of class Calculus.
     */
    @Test
    public void testCarVelocity() {
                System.out.println("Dummy 01");

        System.out.println("carVelocity");
        int rpm = 2482;
        float finalDriverRatio = 2.6F;
        float ratio = 0.9F;
        float expResult = 111.07F;
        float result = Calculus.carVelocity(rpm, finalDriverRatio, ratio);
      
        assertEquals(expResult, result, 0.2);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    
    @Test
    public void testCarVelocity2() {
                System.out.println("Dummy 02");

        System.out.println("carVelocity");
        int rpm = 2317;
        float finalDriverRatio = 2.6F;
        float ratio = 0.7F;
        float expResult = 133.32F;
        float result = Calculus.carVelocity(rpm, finalDriverRatio, ratio);
        
        assertEquals(expResult, result, 0.2);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    @Test
    public void testCarVelocityEletric() {
                System.out.println("Eletric Dummy ");

        System.out.println("carVelocity");
        int rpm = 2482;
        float finalDriverRatio = 2.6F;
        float ratio = 1.0F;
        float expResult = 99.96F;
        float result = Calculus.carVelocity(rpm, finalDriverRatio, ratio);
        
        assertEquals(expResult, result, 0.2);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    
    /**
     * Test of instantFuelConsumption method, of class Calculus.
     */
    @Test
    public void testInstantFuelConsumption() {
                System.out.println("Dummy 01");

        System.out.println("instantFuelConsumption");
        String type = "";
        float SFC = 8.2F;
        double power = 21896.9;
        float time = 600.0F;
        double expResult = 1328703892.0;
        double result = Calculus.instantFuelConsumption(type, SFC, power, time);
        
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of vRelative method, of class Calculus.
     */
    @Test
    public void testVRelative() {
                System.out.println("Dummy 01");

        System.out.println("vRelative");
        float carVelocity = 27.7778F;
        float windSpeed = 3F;
        float windDirection = -5F;
        float expResult = 30.766F;
        float result = Calculus.vRelative(carVelocity, windSpeed, windDirection);
        
        assertEquals(expResult, result, 0.1);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    
    @Test
    public void testVRelative2() {
                System.out.println("Dummy 02");

        System.out.println("vRelative");
        float carVelocity = 33.33F;
        float windSpeed = 3F;
        float windDirection = -5F;
        float expResult = 36.3F;
        float result = Calculus.vRelative(carVelocity, windSpeed, windDirection);
       
        assertEquals(expResult, result, 0.1);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    @Test
    public void testVRelativeEletric() {
                System.out.println("Eletric Dummy ");

        System.out.println("vRelative");
        float carVelocity = 25.0F;
        float windSpeed = 10F;
        float windDirection = -15F;
        float expResult = 34.65F;
        float result = Calculus.vRelative(carVelocity, windSpeed, windDirection);
    
        assertEquals(expResult, result, 0.2);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    
    /**
     * Test of ForceAppliedToTheVehicle method, of class Calculus.
     */
    @Test
    public void testForceAppliedToTheVehicle() {
                System.out.println("Dummy 01");

        System.out.println("ForceAppliedToTheVehicle");
        int torque = 105;   
        float finalDriveRatio = 2.6F;
        float gearRatio = 0.8F;
        float tireRadius = 0.35F;
        float rrc = 0.01F;
        float totalMass = 2400F;
        float frontalArea = 2.4F;
        float drag = 0.38F;
        float slope = 1.5F;
        float windSpeed = 3F;
        float windDirection = -5F;
        float carVelocity =27;
//        float expResult = 0.0F;
      
        float result = Calculus.ForceAppliedToTheVehicle(torque, finalDriveRatio, gearRatio, tireRadius, rrc, totalMass, frontalArea, drag, slope, windSpeed, windDirection,carVelocity);
          // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of kineticEnergy method, of class Calculus.
     */
    @Test
    public void testKineticEnergy() {
                System.out.println("Dummy 01");

        System.out.println("kineticEnergy");
        float mass = 1400F;
        float velocity = 27.77F;
        float expResult = 539821.0F;
        float result = Calculus.kineticEnergy(mass, velocity);
        
        assertEquals(expResult, result, 0.1);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of ForceAppliedToTheVehicleFlatSurface method, of class Calculus.
     */
    @Test
    public void testForceAppliedToTheVehicleFlatSurface() {
                System.out.println("Dummy 01");

        System.out.println("ForceAppliedToTheVehicleFlatSurface");
        int torque = 85;
        float finalDriveRatio = 2.6F;
        float gearRatio = 0.9F;
        float tireRadius = 0.25F;
        float rrc = 0.01F;
        float totalMass = 1400F;
        float frontalArea = 1.8F;
        float drag = 0.35F;
        float windSpeed = 3F;
        float windDirection = -5F;
        float carVelocity = 27.777F;
        float expResult = 293.02F;
        float result = Calculus.ForceAppliedToTheVehicleFlatSurface(torque, finalDriveRatio, gearRatio, tireRadius, rrc, totalMass, frontalArea, drag, windSpeed, windDirection, carVelocity);
      
        assertEquals(expResult, result, 0.2);
        
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    @Test
    public void testForceAppliedToTheVehicleFlatSurface2() {
                System.out.println("Dummy 02");

        System.out.println("ForceAppliedToTheVehicleFlatSurface");
        int torque = 85;
        float finalDriveRatio = 2.6F;
        float gearRatio = 0.7F;
        float tireRadius = 0.25F;
        float rrc = 0.01F;
        float totalMass = 1720F;
        float frontalArea = 1.8F;
        float drag = 0.30F;
        float windSpeed = 3F;
        float windDirection = -5F;
        float carVelocity = 33.33F;
        float expResult = 13.8F;
        float result = Calculus.ForceAppliedToTheVehicleFlatSurface(torque, finalDriveRatio, gearRatio, tireRadius, rrc, totalMass, frontalArea, drag, windSpeed, windDirection, carVelocity);
       
        assertEquals(expResult, result, 0.1);
        
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of regenerationEnergy method, of class Calculus.
     */
    @Test
    public void testRegenerationEnergy() {
        System.out.println("regenerationEnergy");
        float torque = 85F;
        int rpm = 2482;
        float time = 200F;
        float regenerationRatio = 0.8F;
        float expResult = 875876.0F;
        float result = Calculus.regenerationEnergy(torque, rpm, time, regenerationRatio);
        assertEquals(expResult, result, 0.1);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

 
    /**
     * Test of reverseAngle method, of class Calculus.
     */
    @Test
    public void testReverseAngle() {
        System.out.println("reverseAngle");
        float windDirection = 30F;
        float expResult = 150F;
        float result = Calculus.reverseAngle(windDirection);
      
        assertEquals(expResult, result, 0.1);
    }
    @Test
    public void testReverseAngle2() {
        System.out.println("reverseAngle");
        float windDirection = -30F;
        float expResult = -150F;
        float result = Calculus.reverseAngle(windDirection);
        assertEquals(expResult, result, 0.1);
    }


    
}
