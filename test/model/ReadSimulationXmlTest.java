/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.xml.parsers.ParserConfigurationException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.xml.sax.SAXException;

/**
 *
 * @author 1140921
 */
public class ReadSimulationXmlTest {

    public ReadSimulationXmlTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() throws ParserConfigurationException, SAXException {

        String fileName = "TestSet02_Simulation.xml";
        Project project = new Project(null, null);
        InputData r = new ReadNetworkXml();
        r.read(project, "TestSet02_Network.xml");
        InputData road = new ReadSimulationXml();
        road.read(project, fileName);
//        ReadSimulationXml reader = new ReadSimulationXml( project,fileName);
        System.out.println("-->"+project.getSimulations().getSimulations().size());
        Simulation simulation = project.getSimulations().getSimulations().get(0);

        String id = "TestSet02";
//        assertTrue("should be " + id, simulation.getName().equals(id));
//        String description = "simulation test set to begin development";
//        assertTrue("should be ->" + description, simulation.getDescription().equals(description));
        TrafficList lstTraffic = simulation.getLstTraffic();
        assertTrue("shoudl be n0", lstTraffic.getLstTrafficPattern().get(0).getBeg().getId().equals("n0"));
        assertTrue("shoudl be n2", lstTraffic.getLstTrafficPattern().get(0).getEnd().getId().equals("n2"));

        assertTrue("shoudl be n0", lstTraffic.getLstTrafficPattern().get(1).getBeg().getId().equals("n0"));
        assertTrue("shoudl be n4", lstTraffic.getLstTrafficPattern().get(1).getEnd().getId().equals("n4"));

        assertTrue("shoudl be n0", lstTraffic.getLstTrafficPattern().get(2).getBeg().getId().equals("n0"));
        assertTrue("shoudl be n4", lstTraffic.getLstTrafficPattern().get(2).getEnd().getId().equals("n4"));

        assertTrue("shoudl be n1", lstTraffic.getLstTrafficPattern().get(3).getBeg().getId().equals("n1"));
        assertTrue("shoudl be n4", lstTraffic.getLstTrafficPattern().get(3).getEnd().getId().equals("n4"));

        assertTrue("shoudl be n1", lstTraffic.getLstTrafficPattern().get(4).getBeg().getId().equals("n1"));
        assertTrue("shoudl be n4", lstTraffic.getLstTrafficPattern().get(4).getEnd().getId().equals("n4"));

        assertTrue("shoudl be n3", lstTraffic.getLstTrafficPattern().get(5).getBeg().getId().equals("n3"));
        assertTrue("shoudl be n4", lstTraffic.getLstTrafficPattern().get(5).getEnd().getId().equals("n1"));

        assertTrue("shoudl be n3", lstTraffic.getLstTrafficPattern().get(6).getBeg().getId().equals("n3"));
        assertTrue("shoudl be n4", lstTraffic.getLstTrafficPattern().get(6).getEnd().getId().equals("n1"));

        assertTrue("shoudl be n4", lstTraffic.getLstTrafficPattern().get(7).getBeg().getId().equals("n4"));
        assertTrue("shoudl be n4", lstTraffic.getLstTrafficPattern().get(7).getEnd().getId().equals("n1"));

        assertTrue("should be 8", lstTraffic.getLstTrafficPattern().size() == 8);

        int arrivel_values[] = {10, 15, 30, 10, 30, 5, 25, 20};
        int i = 0;
        for (TrafficPattern lst : lstTraffic.getLstTrafficPattern()) {
            assertTrue("should be" + arrivel_values[i], lst.getArrivalRate() == arrivel_values[i]);
            i++;
        }

    }

}
