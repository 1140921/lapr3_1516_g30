package model;

import javax.xml.parsers.ParserConfigurationException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.xml.sax.SAXException;

public class ReadVehiclesXMLTest {

    public ReadVehiclesXMLTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of read method, of class ReadVehiclesXML.
     */
    @Test
    public void testRead() {
        System.out.println("read");
        Project p = new Project("test", "test");
        InputData vec = new ReadVehiclesXML();
        vec.read(p, "TestSet02_Vehicles.xml");
        System.out.println(p.getVehicleList());

        System.out.println("TestSet03");
        Project p1 = new Project("test", "test");
        InputData vec1 = new ReadVehiclesXML();
        vec1.read(p1, "TestSet03_Vehicles.xml");
        System.out.println(p1.getVehicleList());

        assertTrue("should be 3", p1.getVehicleList().size() == 3);
        System.out.println("->" + p1.getVehicleList().getVehicle(2).getEnergy().getEnergy_regeneration_ratio());
//        assertTrue("should be 0.8", p1.getVehicleList().getVehicle(2).getEnergy().getEnergy_regeneration_ratio()==0.8);
        System.out.println("->" + p1.getVehicleList().getVehicle(2).getEnergy().getThrottleLst().getThrottleLst().get(0).getRegimes().get(0).getSFC());
//        assertTrue("should be 0", p1.getVehicleList().getVehicle(2).getEnergy().getThrottleLst().getThrottleLst().get(0).getRegimes().get(0).getSFC()==0);

        assertTrue("Should be Dummy01", p1.getVehicleList().getVehicleList().get(0).getName().equals("Dummy01"));
        assertTrue("Should be Dummy02", p1.getVehicleList().getVehicleList().get(1).getName().equals("Dummy02"));
        assertTrue("Should be ElectricDummy", p1.getVehicleList().getVehicleList().get(2).getName().equals("ElectricDummy"));
        
        
        System.out.println("---->"+p.getVehicleList().getVehicle(1).getVelocityLimitList().getVelocityLimitList());
        System.out.println("---->"+p.getVehicleList().getVehicle(0).getVelocityLimitList().getVelocityLimitList());
    }

    @Test
    public void testDuplicate() {
        System.out.println("read");
        Project p = new Project("test", "test");
        InputData vec = new ReadVehiclesXML();
        vec.read(p, "TestSet02_Vehicles.xml");
//        System.out.println(p.getVehicleList());

        System.out.println("TestSet02(1)");
        InputData vec1 = new ReadVehiclesXML();
        vec1.read(p, "TestSet02_Vehicles(1).xml");

        //Se existir um veiculo com o mesmo nome apenas adiciona mudando o seu nome
        assertTrue("Should be Dummy01", p.getVehicleList().getVehicleList().get(0).getName().equals("Dummy01"));
        assertTrue("Should be Dummy02", p.getVehicleList().getVehicleList().get(1).getName().equals("Dummy02"));
        assertTrue("Should be Dummy01(1)", p.getVehicleList().getVehicleList().get(2).getName().equals("Dummy01(1)"));
        System.out.println(p.getVehicleList().getVehicleList());
        
        
    }
    @Test
    public void testEletricVehicle() {
        System.out.println("read");
        Project p = new Project("test", "test");
        InputData vec = new ReadVehiclesXML();
        vec.read(p, "TestSet03_Vehicles.xml");
// 

        //Se existir um veiculo com o mesmo nome apenas adiciona mudando o seu nome
        System.out.println("-REAL SIZE-->"+p.getVehicleList().getVehicleList().get(1).getEnergy());
        assertTrue("Should be 0.8", p.getVehicleList().getVehicleList().get(2).getEnergy().getEnergy_regeneration_ratio()==0.8f);
        
        
        
    }

}
