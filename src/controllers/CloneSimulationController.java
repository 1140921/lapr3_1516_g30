
package controllers;

import DataLayer.DataHandler;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import model.ProjectRegistry;
import model.Simulation;
import model.SimulationList;


public class CloneSimulationController {
    
    private ProjectRegistry pr;
    private Simulation s;
    private Simulation sClone;
    private SimulationList sl;
    
    public CloneSimulationController(ProjectRegistry pr){
        this.pr=pr;
    }
    
    public void setActiveSimulation(){
        sl=pr.getActiveProject().getSimulations();
        s=sl.getActiveSimulation();
    }
    
    public void cloneSimulation(String name,String description){
        sClone=s.clone(name, description);
    }
    
    public boolean registersSimulation(){
                 DataHandler dh = null;

        try {
            DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
            // URL da BD a usar...
            // ... do servidor local.
            //String jdbcUrl = "jdbc:oracle:thin:@localhost:1521:xe";
            // ... do servidor do DEI.
            String jdbcUrl = "jdbc:oracle:thin://@gandalf.dei.isep.ipp.pt:1521/pdborcl";

            // Credenciais do utilizador da BD.
            String username = "LAPR3_30";
            String password = "fcPorto";
            Connection conn
                    = DriverManager.getConnection(jdbcUrl, username, password);

            dh = new DataHandler(jdbcUrl, username, password);
            conn.setAutoCommit(false);

            System.out.println("\nEstabelecer a ligação à BD...");
            dh.openConnection();
            System.out.println("\t... Ligação obtida.");
            int id=dh.addSimulation(sClone, this.pr.getActiveProject().getId());
            sClone.setId(id);
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("---->" + ex.getMessage());

        } finally {
            String mensagem = dh.closeAll();
            if (!mensagem.isEmpty()) {
                System.out.println(mensagem);
            }
            System.out.println("\nTerminada a ligação à BD.");
        }
        System.out.println("nome: "+sClone.getName());
        return sl.addSimulation(sClone);
    }
}
