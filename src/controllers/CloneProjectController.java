/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import DataLayer.DataHandler;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Project;
import model.ProjectRegistry;
import model.RoadSection;
import model.Vehicle;

/**
 *
 * @author Tixa
 */
public class CloneProjectController {

    private ProjectRegistry projectRegistry;

    public CloneProjectController(ProjectRegistry projectRegistry) {
        this.projectRegistry = projectRegistry;
    }

    public Project cloneProject(String name, String description) {
        Project projectClone = projectRegistry.getActiveProject().clone(name, description);
        return projectClone;
    }

    public boolean addCloneProject(Project project) {
        boolean b = projectRegistry.addProject(project);
        DataHandler dh = null;

        try {
            DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
            // URL da BD a usar...
            // ... do servidor local.
            //String jdbcUrl = "jdbc:oracle:thin:@localhost:1521:xe";
            // ... do servidor do DEI.
            String jdbcUrl = "jdbc:oracle:thin://@gandalf.dei.isep.ipp.pt:1521/pdborcl";

            // Credenciais do utilizador da BD.
            String username = "LAPR3_30";
            String password = "fcPorto";
            Connection conn
                    = DriverManager.getConnection(jdbcUrl, username, password);

            dh = new DataHandler(jdbcUrl, username, password);
            conn.setAutoCommit(false);

            System.out.println("\nEstabelecer a ligação à BD...");
            dh.openConnection();
            System.out.println("\t... Ligação obtida.");

            int id=dh.addProject(project);
            project.setId(id);
            dh.addVehicle(project.getVehicleList().getVehicleList(), project.getId());
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("---->" + ex.getMessage());

        } finally {
            String mensagem = dh.closeAll();
            if (!mensagem.isEmpty()) {
                System.out.println(mensagem);
            }
            System.out.println("\nTerminada a ligação à BD.");
        }
        return b;
    }

}
