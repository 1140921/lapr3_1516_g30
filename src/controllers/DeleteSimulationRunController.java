
package controllers;

import java.util.List;
import model.ProjectRegistry;
import model.Simulation;
import model.SimulationRun;

public class DeleteSimulationRunController {

    private Simulation activeSimulation;
    
    private SimulationRun sr;
    
    public DeleteSimulationRunController(ProjectRegistry pr){
        activeSimulation=pr.getActiveProject().getSimulations().getActiveSimulation();
    }
    
    public List<SimulationRun>getSimulationRuns(){
        return this.activeSimulation.getSimulationRuns().getSimulationRuns();
        
    }
    public void setSimulationRun(SimulationRun sr){
        this.sr=sr;
    }
    
    public boolean deleteSimulationRun(){
        return this.activeSimulation.getSimulationRuns().removeSimulation(sr);
    }
    
    
}
