/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import DataLayer.DataHandler;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import model.ProjectRegistry;
import model.Project;

/**
 *
 * @author hichampt
 */
public class EditProjectPropertiesController {

    private Project projectActive;
    private ProjectRegistry pLst;

    
    public EditProjectPropertiesController(ProjectRegistry pLst) {
        this.pLst = pLst;
    }
    
    /**
     * 
     * @param strNome project to modify.
     * @return 
     */
    public Project getProject() {
        return this.pLst.getActiveProject();
    }
    
   

    public boolean modifyData(String name, String description) {
     
                DataHandler dh =null;
         try {
            DriverManager.registerDriver(new oracle.jdbc.OracleDriver());

            String jdbcUrl = "jdbc:oracle:thin://@gandalf.dei.isep.ipp.pt:1521/pdborcl";
            String username = "LAPR3_30";
            String password = "fcPorto";
            Connection conn
                    = DriverManager.getConnection(jdbcUrl, username, password);

            dh = new DataHandler(jdbcUrl, username, password);
            conn.setAutoCommit(false);
            
            System.out.println("\nEstabelecer a ligação à BD...");
            dh.openConnection();
            System.out.println("\t... Ligação obtida.");
            dh.setProject(pLst.getActiveProject().getId(), name, description);
         } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("---->" + ex.getMessage());

        } finally {
            String mensagem = dh.closeAll();
            if (!mensagem.isEmpty()) {
                System.out.println(mensagem);
            }
            System.out.println("\nTerminada a ligação à BD.");
        }
      return this.pLst.modifiyData(name, description);
        

    }

}
