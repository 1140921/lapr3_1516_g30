/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import DataLayer.DataHandler;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import model.InputData;
import model.Project;
import model.ProjectRegistry;
import model.ReadNetworkXml;
import model.RoadSection;
import model.Vehicle;
import org.xml.sax.SAXException;

/**
 *
 * @author António
 */
public class CreateProjectController {
        
    private String filePath;
    private Project project;
    private ProjectRegistry projectRegistry;
    private String name;
    private String description;  

    public CreateProjectController(ProjectRegistry projectRegistry) {
        this.projectRegistry = projectRegistry;
    }

    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * @param filePath the filePath to set
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void CreateProject(String name, String description) {
        
            this.project = new Project(name, description);
        

    }

    /**
     *
     *
     *
     * This method creates a new Project and adds the new Project to the List of
     * Projects in the ProjectRegistry
     *
     * @throws ParserConfigurationException
     * @throws SAXException
     */
    public void CreateNewProject() throws ParserConfigurationException, SAXException {
//        this.project= new Project("", "");
        InputData roads = new ReadNetworkXml();
        roads.read(project, filePath);
        projectRegistry.getProjectList().add(project); 
         DataHandler dh = null;

        try {
            DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
            // URL da BD a usar...
            // ... do servidor local.
            //String jdbcUrl = "jdbc:oracle:thin:@localhost:1521:xe";
            // ... do servidor do DEI.
            String jdbcUrl = "jdbc:oracle:thin://@gandalf.dei.isep.ipp.pt:1521/pdborcl";

            // Credenciais do utilizador da BD.
            String username = "LAPR3_30";
            String password = "fcPorto";
            Connection conn
                    = DriverManager.getConnection(jdbcUrl, username, password);

            dh = new DataHandler(jdbcUrl, username, password);
            conn.setAutoCommit(false);

            System.out.println("\nEstabelecer a ligação à BD...");
            dh.openConnection();
            System.out.println("\t... Ligação obtida.");

            int id=dh.addProject(project);
            project.setId(id);
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("---->" + ex.getMessage());

        } finally {
            String mensagem = dh.closeAll();
            if (!mensagem.isEmpty()) {
                System.out.println(mensagem);
            }
            System.out.println("\nTerminada a ligação à BD.");
        }
    }
}
