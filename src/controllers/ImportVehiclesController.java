
package controllers;

import DataLayer.DataHandler;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import model.InputData;
import model.Project;
import model.ProjectRegistry;
import model.ReadVehiclesXML;
import org.xml.sax.SAXException;


public class ImportVehiclesController {
    
    private ProjectRegistry pr;
    private String path;
    public ImportVehiclesController(ProjectRegistry pr){
        this.pr=pr;
    }
    
    public void setPath(String path){
        this.path=path;
    }
    public void importFile() throws ParserConfigurationException, SAXException{
        InputData vec= new ReadVehiclesXML();
        vec.read(pr.getActiveProject(),this.path);
         DataHandler dh = null;

        try {
            DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
            // URL da BD a usar...
            // ... do servidor local.
            //String jdbcUrl = "jdbc:oracle:thin:@localhost:1521:xe";
            // ... do servidor do DEI.
            String jdbcUrl = "jdbc:oracle:thin://@gandalf.dei.isep.ipp.pt:1521/pdborcl";

            // Credenciais do utilizador da BD.
            String username = "LAPR3_30";
            String password = "fcPorto";
            Connection conn
                    = DriverManager.getConnection(jdbcUrl, username, password);

            dh = new DataHandler(jdbcUrl, username, password);
            conn.setAutoCommit(false);

            System.out.println("\nEstabelecer a ligação à BD...");
            dh.openConnection();
            System.out.println("\t... Ligação obtida.");
            
            dh.addVehicle(pr.getActiveProject().getVehicleList().getVehicleList(), pr.getActiveProject().getId());
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("---->" + ex.getMessage());

        } finally {
            String mensagem = dh.closeAll();
            if (!mensagem.isEmpty()) {
                System.out.println(mensagem);
            }
            System.out.println("\nTerminada a ligação à BD.");
        }
    }
}
