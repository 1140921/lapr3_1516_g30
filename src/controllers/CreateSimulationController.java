
package controllers;

import DataLayer.DataHandler;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import model.InputData;
import model.Project;
import model.ProjectRegistry;
import model.ReadSimulationXml;
import model.Simulation;
import model.SimulationList;


public class CreateSimulationController {
    
    private ProjectRegistry pr;
    private SimulationList sl;
    private Project p;
    private Simulation s;
    
    public CreateSimulationController(ProjectRegistry pr){
        this.pr=pr;
    }
    
    public void CreateSimulation(String name,String description){
        p=pr.getActiveProject();
        sl=p.getSimulations();
        s=sl.newSimulation(name, description);
    }
    
    public void ImportTrafficProfileData(String path){
        InputData trafficRead = new ReadSimulationXml();
        Simulation aux=p.getSimulations().getActiveSimulation();
        p.getSimulations().setActiveSimulation(s);
        trafficRead.read(p, path);
        p.getSimulations().setActiveSimulation(aux);
    }
    
    public boolean registerSimulation(){
       DataHandler dh = null;

        try {
            DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
            // URL da BD a usar...
            // ... do servidor local.
            //String jdbcUrl = "jdbc:oracle:thin:@localhost:1521:xe";
            // ... do servidor do DEI.
            String jdbcUrl = "jdbc:oracle:thin://@gandalf.dei.isep.ipp.pt:1521/pdborcl";

            // Credenciais do utilizador da BD.
            String username = "LAPR3_30";
            String password = "fcPorto";
            Connection conn
                    = DriverManager.getConnection(jdbcUrl, username, password);

            dh = new DataHandler(jdbcUrl, username, password);
            conn.setAutoCommit(false);
            
            System.out.println("\nEstabelecer a ligação à BD...");
            dh.openConnection();
            System.out.println("\t... Ligação obtida.");
            int id=dh.addSimulation(s, p.getId());
            this.s.setId(id);
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("---->" + ex.getMessage());

        } finally {
            String mensagem = dh.closeAll();
            if (!mensagem.isEmpty()) {
                System.out.println(mensagem);
            }
            System.out.println("\nTerminada a ligação à BD.");
        }
        boolean b=  sl.addSimulation(s);
        return b;
    }

}
