package controllers;

import DataLayer.DataHandler;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Edge;
import model.InputData;
import model.Junction;
import model.Project;
import model.ProjectRegistry;
import model.ReadNetworkXml;
import model.RoadSection;
import model.Vertex;

public class AddRoadsToProjectController {

    private ProjectRegistry pr;

    private Project activeProject;
    
    private RoadSection roadSection;
    List<RoadSection> newRoadsections;
    
    InputData fileReader = new ReadNetworkXml();

    public AddRoadsToProjectController(ProjectRegistry pr) {
        this.pr = pr;
        this.activeProject = pr.getActiveProject();
    }

    public Project getActiveProject() {
        return activeProject;
    }

    public String getActiveProjectName() {
        return activeProject.getName();
    }

    public String getActiveProjectDescription() {
        return activeProject.getDescription();
    }

    public String getActiveProjectRoads() {
        String stringBuilder = " ";
        for (Edge<Junction, List<RoadSection>> r : activeProject.getRoadNetwork().edges()) {
            stringBuilder = r.getElement().toString();
        }
        return stringBuilder;
    }

    public void importFile(String path) {
        fileReader.read(activeProject, path);    
    }
    
    public List<RoadSection> getRoadSection(){
        List<RoadSection> list = new ArrayList<>();
        for(Edge<Junction, List<RoadSection>> roads :activeProject.getRoadNetwork().edges()){
          list = roads.getElement();
        }
        return list;
    }
    
    public List<Junction> getJunctions(){
          List<Junction> list = new ArrayList<>();
        for(Vertex<Junction, List<RoadSection>> junctions :activeProject.getRoadNetwork().vertices()){
          list.add(junctions.getElement());
        }
        return list;
    }
    
    public void setnewRoadsections(List<RoadSection> r){
        newRoadsections=r;
    }
    
}
