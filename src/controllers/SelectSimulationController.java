/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import DataLayer.DataHandler;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import model.Project;
import model.ProjectRegistry;
import model.Simulation;

/**
 *
 * @author 1140921
 */
public class SelectSimulationController {
    
    private ProjectRegistry pr;
    
    public SelectSimulationController(ProjectRegistry pr){
        this.pr=pr;
        DataHandler dh =null;
         try {
            DriverManager.registerDriver(new oracle.jdbc.OracleDriver());

            String jdbcUrl = "jdbc:oracle:thin://@gandalf.dei.isep.ipp.pt:1521/pdborcl";
            String username = "LAPR3_30";
            String password = "fcPorto";
            Connection conn
                    = DriverManager.getConnection(jdbcUrl, username, password);

            dh = new DataHandler(jdbcUrl, username, password);
            conn.setAutoCommit(false);
            
            System.out.println("\nEstabelecer a ligação à BD...");
            dh.openConnection();
            System.out.println("\t... Ligação obtida.");
            this.pr.getActiveProject().getSimulations().setSimulations(dh.getSimulation(pr.getActiveProject()));
        
         } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("---->" + ex.getMessage());

        } finally {
            String mensagem = dh.closeAll();
            if (!mensagem.isEmpty()) {
                System.out.println(mensagem);
            }
            System.out.println("\nTerminada a ligação à BD.");
        }
    
    
    }
    
    public List<Simulation> getSimulationList(){
        return this.pr.getActiveProject().getSimulations().getSimulations();
    }
    
    public void selectSimulation(Simulation simulation){
        this.pr.getActiveProject().getSimulations().setActiveSimulation(simulation);
    }
    
}
