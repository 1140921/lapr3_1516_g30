/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.HashMap;
import model.Project;
import model.ProjectRegistry;
import model.Simulation;
import model.SimulationResults;
import model.SimulationRunList;
import model.TrafficList;
import model.TrafficPattern;

/**
 *
 * @author António
 */
public class SimulationResultsController {
//    private SimulationResults simulationResults;
    private ProjectRegistry projectRegistry;
    private Project projectActive;
    private Simulation simulation;
    private SimulationRunList simulationRuns;
    private int index;
    
    public SimulationResultsController(ProjectRegistry projectRegistry){
        this.projectActive = projectRegistry.getActiveProject();
        this.simulation = this.projectActive.getSimulations().getActiveSimulation();
        this.simulationRuns = this.simulation.getSimulationRuns();
//        this.simulationResults = this.simulation.getSimulationRuns().getSimulationRuns();
    }
//    public SimulationResults getSimulationResults(){
//        this.simulationResults = this.simulation.getSimulationRuns().getSimulationRuns().get(this.index).getSimulationResults();
//        return this.simulationResults;
//    }
    public HashMap<TrafficPattern, Float> getAverageEnergyConsumptionForEachTraffic(TrafficList trafficPatternList){
        HashMap<TrafficPattern, Float> averageEnergyConsumption = this.getSimulationRuns().getSimulationRuns().get(index).getSimulationResults().averageEnergyConsumptionForEachTraffic(trafficPatternList);
//        HashMap<TrafficPattern, Float> averageEnergyConsumption = simulationResults.averageEnergyConsumptionForEachTraffic(trafficPatternList);
        return averageEnergyConsumption;
    }
    
    public HashMap<TrafficPattern, Float> getAverageEnergyConsumptionForEachTrafficInAllSgments(TrafficList trafficPatternList){
        HashMap<TrafficPattern, Float> averageEnergyConsumption = this.getSimulationRuns().getSimulationRuns().get(index).getSimulationResults().averageEnergyConsumptionForEachTrafficInAllSgements(trafficPatternList);
//        HashMap<TrafficPattern, Float> averageEnergyConsumption = simulationResults.averageEnergyConsumptionForEachTrafficInAllSgements(trafficPatternList);
        return averageEnergyConsumption;
    }
    
    public void setIndexResults(int index){
        this.index = index;
    }

    /**
     * @return the simulationRuns
     */
    public SimulationRunList getSimulationRuns() {
        return simulationRuns;
    }

    /**
     * @return the projectActive
     */
    public Project getProjectActive() {
        return projectActive;
    }

    /**
     * @return the simulation
     */
    public Simulation getSimulation() {
        return simulation;
    }
    
    
}
