package controllers;

import java.util.ArrayList;
import java.util.List;
import model.BestPath;
import model.EfficientPathRealConditions;
import model.FastestPath;
import model.Graph;
import model.Junction;
import model.NetworkAnalisysResults;
import model.Project;
import model.ProjectRegistry;
import model.RoadSection;
import model.TheoreticalEfficientPath;
import model.Vehicle;
import model.Vertex;

public class AnalyseBestPathController {
    
    private ProjectRegistry projectRegistry;
    private Junction beginingNode;
    private Junction endingNode;
    private Graph<Junction,List<RoadSection>> roadNetwork;
    private Vehicle vehicle;
    private BestPath algorithm;

    public AnalyseBestPathController(ProjectRegistry projectRegistry) {
        this.projectRegistry=projectRegistry;
        this.roadNetwork = projectRegistry.getActiveProject().getRoadNetwork();
    }

    public List<Junction> getNodeList() {
        List<Junction> nodeList = new ArrayList<>();
        for (Vertex<Junction, List<RoadSection>> j : roadNetwork.vertices()) {
            nodeList.add(j.getElement());
        }
        return nodeList;
    }
    
    public List<Vehicle> getVehicleList(){
      return   projectRegistry.getActiveProject().getVehicleList().getVehicleList();   
    }
    public List<BestPath> getBestPathAlogorithms(){
        BestPath path1= new FastestPath();
        BestPath path2 = new TheoreticalEfficientPath();
        BestPath path3= new EfficientPathRealConditions();
        List<BestPath> bestPaths= new ArrayList<>();
        bestPaths.add(path1);
        bestPaths.add(path2);
        bestPaths.add(path3);
        return  bestPaths;
    }
    
    public void setBeginingNode(Junction beginingNode) {
        this.beginingNode = beginingNode;
        
    }

    public void setEndingNode(Junction endingNode) {
        this.endingNode = endingNode;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public void setAlgorithm(BestPath algorithm) {
        this.algorithm = algorithm;
    }

    public NetworkAnalisysResults executeAlgorithm() {
        return this.algorithm.getPath(roadNetwork, beginingNode, endingNode, vehicle);
    }
}
