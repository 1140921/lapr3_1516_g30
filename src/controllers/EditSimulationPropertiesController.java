
package controllers;

import DataLayer.DataHandler;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import model.ProjectRegistry;
import model.Simulation;


public class EditSimulationPropertiesController {
    
    private ProjectRegistry pr;
    private Simulation simulation;
    
    public EditSimulationPropertiesController(ProjectRegistry pr){
        this.pr=pr;
        this.simulation=pr.getActiveProject().getSimulations().getActiveSimulation();
        System.out.println("---------------ads------------------"+simulation.toString());
    }
    
    public String[] getProperties(){
        simulation=pr.getActiveProject().getSimulations().getActiveSimulation();
        String[] str=new String[2];
        str[0]=simulation.getName();
        str[1]=simulation.getDescription();
        return str;
    }
    
    public void modifyProperties(String name,String description){
        simulation.setName(name);
        simulation.setDescription(description);
                 DataHandler dh = null;

        try {
            DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
            // URL da BD a usar...
            // ... do servidor local.
            //String jdbcUrl = "jdbc:oracle:thin:@localhost:1521:xe";
            // ... do servidor do DEI.
            String jdbcUrl = "jdbc:oracle:thin://@gandalf.dei.isep.ipp.pt:1521/pdborcl";

            // Credenciais do utilizador da BD.
            String username = "LAPR3_30";
            String password = "fcPorto";
            Connection conn
                    = DriverManager.getConnection(jdbcUrl, username, password);

            dh = new DataHandler(jdbcUrl, username, password);
            conn.setAutoCommit(false);

            System.out.println("\nEstabelecer a ligação à BD...");
            dh.openConnection();
            System.out.println("\t... Ligação obtida.");
            dh.setSimulation(this.simulation.getId(),name, description);
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("---->" + ex.getMessage());

        } finally {
            String mensagem = dh.closeAll();
            if (!mensagem.isEmpty()) {
                System.out.println(mensagem);
            }
            System.out.println("\nTerminada a ligação à BD.");
        }
    }
}
