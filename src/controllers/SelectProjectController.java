
package controllers;

import model.Project;
import model.ProjectRegistry;
import java.util.List;


public class SelectProjectController {

    private ProjectRegistry project;
    
    public SelectProjectController(ProjectRegistry project){
        this.project=project;
    }

    public List<Project> getProjectsList(){
        return this.project.getProjectList();
    }
    
    public void setActiveProjec(Project project){
        this.project.setActiveProject(project);
    }

}
