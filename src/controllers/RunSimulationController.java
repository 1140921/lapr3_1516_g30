/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.util.ArrayList;
import java.util.List;
import model.BestPath;
import model.EfficientPathRealConditions;
import model.FastestPath;
import model.Graph;
import model.Junction;
import model.NetworkAnalisysResults;
import model.Project;
import model.ProjectRegistry;
import model.RoadSection;
import model.TheoreticalEfficientPath;
import model.TrafficPattern;
import model.TrafficList;
import model.Vehicle;

/**
 *
 * @author 1140921
 */
public class RunSimulationController {
    
      private ProjectRegistry projectRegistry;
      private Project activeProject;
    private Junction beginingNode;
    private Junction endingNode;
    private Graph<Junction,List<RoadSection>> roadNetwork;
    private Vehicle vehicle;
    private BestPath algorithm;

    public RunSimulationController(ProjectRegistry projectRegistry) {
        this.projectRegistry=projectRegistry;
//        this.roadNetwork = projectRegistry.getActiveProject().getRoadNetwork();
    }
      public List<BestPath> getBestPathAlogorithms(){
        BestPath path1= new FastestPath();
        BestPath path3= new EfficientPathRealConditions();
        List<BestPath> bestPaths= new ArrayList<>();
        bestPaths.add(path1);
        bestPaths.add(path3);
        return  bestPaths;
    }
      
      public String getNameSimulationName (){
         return this.activeProject.getSimulations().getActiveSimulation().getName();
      }
      public String getNameSimulationDuration(){
         return this.activeProject.getSimulations().getActiveSimulation().getDescription();
      }
//      public List<String> getNameSimulationTimeStep(){
//          List<String> l = new ArrayList();
//          for(TrafficPattern lstP : this.activeProject.getSimulations().getActiveSimulation().getLstTraffic().getLstTrafficPattern()){
//              l.add(lstP.getArrivalRate());
//          }
//         return this.activeProject.getSimulations().getActiveSimulation().getLstTraffic().getLstTrafficPattern().ge;
//      }
      
    
      public NetworkAnalisysResults executeAlgorithm() {
        return this.algorithm.getPath(roadNetwork, beginingNode, endingNode, vehicle);
    }
}
