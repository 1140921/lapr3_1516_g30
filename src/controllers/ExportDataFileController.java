/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import model.ExportDataFile;
import model.NetworkAnalisysResults;

/**
 *
 * @author hichampt
 */
public class ExportDataFileController {

    private ExportDataFile ex;

    public ExportDataFileController() {

        ex = new ExportDataFile();
    }

    public void criarHTMLpt(File file) throws FileNotFoundException, UnsupportedEncodingException, IOException {

        try {
            ex.criarHTML(file);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(ExportDataFileController.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    
     public void createHTMLCompareVehicle(File file, List<NetworkAnalisysResults> lst) throws FileNotFoundException, UnsupportedEncodingException, IOException {
         
        try {
            ex.createHTMLCompareVehicle(file, lst);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(ExportDataFileController.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    
    

}
