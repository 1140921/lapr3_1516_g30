package DataLayer;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import model.Direction;
import model.Energy;
import model.Gear;
import model.Junction;
import model.Project;
import model.Regime;
import model.RoadSection;
import model.Segment;
import model.Simulation;
import model.Throttle;
import model.TrafficPattern;
import model.Vehicle;
import model.VelocityLimit;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
import oracle.jdbc.pool.OracleDataSource;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

/**
 * Exemplo de classe cujas instâncias manipulam dados de BD Oracle.
 */
public class DataHandler {

    /*******************************VARIÁVEIS**********************************/
    /**
     * O URL da BD.
     */
    private String jdbcUrl;

    /**
     * O nome de utilizador da BD.
     */
    private String username;

    /**
     * A password de utilizador da BD.
     */
    private String password;

    /**
     * A ligação à BD.
     */
    private Connection connection;

    /**
     * A invocação de "stored procedures".
     */
    private CallableStatement callStmt;

    /**
     * Conjunto de resultados retornados por "stored procedures".
     */
    private ResultSet rSet;

    
    

    
    
    /*******************************CONSTRUTOR*********************************/
    /**
     * Constrói uma instância de "DataHandler" recebendo, por parâmetro, o URL
     * da BD e as credenciais do utilizador.
     *
     * @param jdbcUrl o URL da BD.
     * @param username o nome do utilizador.
     * @param password a password do utilizador.
     */
    public DataHandler(String jdbcUrl, String username, String password) {
        this.jdbcUrl = jdbcUrl;
        this.username = username;
        this.password = password;
        connection = null;
        callStmt = null;
        rSet = null;
    }
    
    
    /******************************METODOS LIGACAO BD**************************/
    /**
     * Estabelece a ligação à BD.
     */
    public void openConnection() throws SQLException {
        OracleDataSource ds = new OracleDataSource();
        ds.setURL(jdbcUrl);
        connection = ds.getConnection(username, password);
    }

    /**
     * Fecha os objetos "ResultSet", "CallableStatement" e "Connection", e
     * retorna uma mensagem de erro se alguma dessas operações não for bem
     * sucedida. Caso contrário retorna uma "string" vazia.
     */
    public String closeAll() {

        StringBuilder message = new StringBuilder("");

        if (rSet != null) {
            try {               
                rSet.close();
            } catch (SQLException ex) {               
                message.append(ex.getMessage());
                message.append("\n");
            }
            rSet = null;
        }

        if (callStmt != null) {
            try {
                callStmt.close();
            } catch (SQLException ex) {               
                message.append(ex.getMessage());
                message.append("\n");
            }
            callStmt = null;
        }

        if (connection != null) {
            try {                 
                connection.close();
            } catch (SQLException ex) {                
                message.append(ex.getMessage());
                message.append("\n");
            }
            connection = null;
        }        
        return message.toString();       
    }
    
    
    /**************************GET*****************************************/   
       
      
   public ArrayList<Project> getProjects ()  throws SQLException {
       
       
       ArrayList<Project> arrayProj =new ArrayList<>();  
       
       
       //ResultSet
        ResultSet rSet2, rSet3, rSet4, rSet5, rSet6, rSet7, rSet8, rSet9;
        
       
       
       //PROJECT
       callStmt = connection.prepareCall("{ ? = call getTPROJECT}");
       callStmt.registerOutParameter(1, OracleTypes.CURSOR);
       callStmt.execute();
       rSet = (ResultSet) callStmt.getObject(1);//tudo project
       System.out.println(" carregar projects...");        
               
        while(rSet.next()){ //project
            Project p=new Project(rSet.getInt(1),rSet.getString(2), rSet.getString(3));
           
            
            //ROADSECTION
            callStmt = connection.prepareCall("{ ? = call getROADSECTION (?)}");
            callStmt.registerOutParameter(1, OracleTypes.CURSOR);
            callStmt.setInt(2, rSet.getInt(1)); // id_project
            callStmt.execute();
            
            
            rSet2 = (ResultSet) callStmt.getObject(1); //tudo roadSection          
            
            while(rSet2.next()){ //roadsection
                Direction d;
                if(rSet2.getString(8).equalsIgnoreCase(Direction.DIRECT.name())){
                    d=Direction.DIRECT;
                }else
                    d=Direction.BIDIRECTIONAL;
                
                RoadSection rs= new RoadSection (rSet2.getString(1),
                                                    new Junction(rSet2.getString(6)),
                                                    new Junction(rSet2.getString(7)),    
                                                    rSet2.getString(2),
                                                    d,                                                    
                                                    rSet2.getFloat(3),                                                    
                                                    rSet2.getFloat(4),
                                                    rSet2.getFloat(5));   
                
                p.addJunction(rs.getBeginningNode());
                p.addJunction(rs.getEndingNode());
                
                //System.out.println(rs.toString());                
               
               // System.out.println("");
                
                               
                //SEGMENT
                callStmt = connection.prepareCall("{ ? = call getSEGMENT (?, ?, ?, ?)}");
                callStmt.registerOutParameter(1, OracleTypes.CURSOR);
                callStmt.setString(2, rSet2.getString(1)); // id_roadSection
                callStmt.setString(3, rSet2.getString(7)); // edingNode
                callStmt.setString(4, rSet2.getString(6)); //beginningNode
                callStmt.setInt(5, rSet.getInt(1)); //id_project
                
                callStmt.execute();                  
                
                rSet3 = (ResultSet) callStmt.getObject(1);  //tudo segment
                while(rSet3.next()){ //segment
                     
                  Segment segm=new Segment (rSet3.getString(1),
                                            rSet3.getFloat(2),
                                            rSet3.getFloat(3),
                                            rSet3.getFloat(4),
                                            rSet3.getFloat(5),
                                            rSet3.getFloat(6),
                                            rSet3.getInt(7));                    
                    //System.out.println("------------------------seg,emttt");
                    //System.out.println(segm.toString());
                    //System.out.println("-----------------------------");
                    
                  rs.getSegmentList().addSegment(segm);                    
//                  System.out.println(rs);                    
                    
                 }if (arrayProj.isEmpty()) {
                    System.out.println("No segment found.");
                }else{
                    System.out.println("segment loaded succesfully.");
                }  
                 //System.out.println("---------TESTES----------------------------");
               rSet3.close();
               p.addSection(rs);
                                     
            }if (arrayProj.isEmpty()) {
                System.out.println("No roadsection found.");
            }else{
                System.out.println("RoadSection loaded succesfully.");
            }    
            
           rSet2.close();
            //VEHICLE
            callStmt = connection.prepareCall("{ ? = call getVEHICLE (?)}");
            callStmt.registerOutParameter(1, OracleTypes.CURSOR);
            callStmt.setInt(2, rSet.getInt(1)); // id_project
            callStmt.execute(); 
            rSet4 = (ResultSet) callStmt.getObject(1);  //tudo vehicle
            

            
            
            while(rSet4.next()){ //vehicle
             
                Vehicle vehicle= new Vehicle(
                                        rSet4.getInt(13),
                                        rSet4.getString(1),
                                        rSet4.getString(3),
                                        rSet4.getString(4),
                                        rSet4.getString(5),
                                        rSet4.getString(6),
                                        rSet4.getFloat(7),
                                        rSet4.getFloat(8),
                                        rSet4.getFloat(9),
                                        rSet4.getFloat(10),
                                        rSet4.getFloat(11),
                                        rSet4.getFloat(12));
                
                
                //System.out.println(vehicle.toString());
                
                //System.out.println(""); 
                
                            //ENERGY
            callStmt = connection.prepareCall("{ ? = call getENERGY (?)}");
            callStmt.registerOutParameter(1, OracleTypes.CURSOR);
            callStmt.setInt(2, rSet4.getInt(13)); // vehicle_id
            callStmt.execute();             
            rSet6 = (ResultSet) callStmt.getObject(1);  //tudo ENERGY
            
                rSet6.next();
                 Energy energy=new Energy(rSet6.getInt(2),
                                           rSet6.getInt(3),
                                           rSet6.getFloat(4),
                                           rSet6.getFloat(5));

                   //System.out.println(energy.toString());                   
                   //System.out.println(""); 
                   
            rSet6.close();     
                
                //VElOCITY LIMIT
                callStmt = connection.prepareCall("{ ? = call getVELOCITY_LIMIT (?)}");
                callStmt.registerOutParameter(1, OracleTypes.CURSOR);
                callStmt.setInt(2, rSet4.getInt(13)); // vehicle_id
                callStmt.execute(); 

                rSet5 = (ResultSet) callStmt.getObject(1);  //tudo VElOCITY LIMIT
                while(rSet5.next()){ //VElOCITY LIMIT

                    VelocityLimit velocityLimit=new VelocityLimit(rSet5.getString(3), rSet5.getInt(4));

                    //System.out.println(velocityLimit.toString());                    
                    //System.out.println(""); 
                    vehicle.getVelocityLimitList().addVelocityLimit(velocityLimit);
                    

                 }if (arrayProj.isEmpty()) {
                        System.out.println("No velocity limit found.");
                }else{
                        System.out.println("velocity limit loaded succesfully.");
                }               
                 rSet5.close(); 


                   //GEAR
                   callStmt = connection.prepareCall("{ ? = call getGEAR (?)}");
                   callStmt.registerOutParameter(1, OracleTypes.CURSOR);
                   callStmt.setInt(2, rSet4.getInt(13)); // vehicle_id
                   callStmt.execute(); 

                   rSet7 = (ResultSet) callStmt.getObject(1);  //tudo gear
                   while(rSet7.next()){ //gear

                       Gear gear=new Gear(rSet7.getString(1), rSet7.getFloat(3));

                       //System.out.println(gear.toString());
                       
                       //System.out.println(""); 
                       energy.getGearList().addGear(gear);

                     }if (arrayProj.isEmpty()) {
                           System.out.println("No gear found.");
                     }else{
                           System.out.println("gear loaded succesfully.");
                    } 
                     rSet7.close();

                    //Throttle
                   callStmt = connection.prepareCall("{ ? = call getTHROTTLE (?)}");
                   callStmt.registerOutParameter(1, OracleTypes.CURSOR);
                   callStmt.setInt(2, rSet4.getInt(13)); // vehicle_id
                   callStmt.execute(); 

                   rSet8 = (ResultSet) callStmt.getObject(1);  //tudo throttle
                   while(rSet8.next()){ //trottle

                       Throttle throttle=new Throttle(rSet8.getString(1));

                       //System.out.println(throttle.toString());                       
                       //System.out.println(""); 


                       //REGIME
                       callStmt = connection.prepareCall("{ ? = call getREGIME (?, ?)}");
                       callStmt.registerOutParameter(1, OracleTypes.CURSOR);
                       callStmt.setInt(2, rSet4.getInt(13)); // vehicle_id
                       callStmt.setString(3, rSet8.getString(1)); // cod_Throttle
                       callStmt.execute(); 

                       rSet9 = (ResultSet) callStmt.getObject(1);  //tudo regime
                       while(rSet9.next()){ //regime

                           Regime regime=new Regime(rSet9.getInt(4),
                                                    rSet9.getInt(5),
                                                    rSet9.getInt(6),
                                                    rSet9.getFloat(7));

                           //System.out.println(regime.toString());
                           throttle.getRegimes().add(regime);
                          // System.out.println(""); 

                         }if (arrayProj.isEmpty()) {
                               System.out.println("Regime Not  found.");
                         }else{
                               System.out.println("Regime loaded succesfully.");
                        }     
                         rSet9.close();
                        energy.getThrottleLst().addThrottle(throttle);

                     }if (arrayProj.isEmpty()) {
                           System.out.println("No throttle found.");
                     }else{
                           System.out.println("throttle loaded succesfully.");
                    }     
                     rSet8.close();
                
                    vehicle.setEnergy(energy);
                    p.getVehicleList().addVehicle(vehicle);
                
             }if (arrayProj.isEmpty()) {
                    System.out.println("No vehicle found.");
             }else{
                    System.out.println("vehicle loaded succesfully.");
            }             
                         
             
           arrayProj.add(p); 
        }
        if (arrayProj.isEmpty()) {
            System.out.println("No projects found.");
        }else{
            System.out.println("Projects loaded succesfully.");
        }    
       rSet.close();
       return arrayProj;
   }
   
   

   
   
   
    
    
    /**************************ADD******************************************/
    //ADDPROJECT - inserir projectos BD
    public int addProject (Project p) throws SQLException {
       int id_project=0;
       
       connection.setAutoCommit(false);
       try {
        callStmt=connection.prepareCall("{call ADD_PROJECT (?, ?, ?) }");
              
        // Especifica o parâmetro de entrada da função
        callStmt.setString(1, p.getName());
        callStmt.setString(2, p.getDescription());         
        callStmt.registerOutParameter(3, OracleTypes.NUMBER);       
        
        // Executa a invocação da função/proc
        callStmt.execute();
        
        // Guarda o cursor retornado num objeto "ResultSet".
        id_project = callStmt.getInt(3);  
        
        
           addRoadSection(p.getSections(), id_project);
           for(RoadSection rs:p.getSections()){
                   addSegment(rs.getSegmentList().getSegmentList(),rs.getRoadId(),rs.getEndingNode().getId(), rs.getBeginningNode().getId(), id_project);
           }
        
        connection.commit();
       }
       catch (Exception e){
           connection.rollback();           
       }              
        return id_project;
    }
   
    
    
    
   

    //ADDROADSECTION - inserir roadsection na BD        
    public void addRoadSection (List <RoadSection> roadSectionLst, int id_Proj) throws SQLException {
        
        connection.setAutoCommit(false);
        try {
            
            ArrayDescriptor oracleVarcharCollection = ArrayDescriptor.createDescriptor("VARCHAR_T",connection);
            ArrayDescriptor oracleIntegerCollection = ArrayDescriptor.createDescriptor("INTEGER_T",connection);
            ArrayDescriptor oracleNumberCollection = ArrayDescriptor.createDescriptor("NUMBER_T",connection);
            
            callStmt=connection.prepareCall("{ call insert_roadSection(?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");
                      
            // JAVA arrays to hold the data.
            String [] road_id_array=new String[roadSectionLst.size()];
            String [] typology_array=new String[roadSectionLst.size()];
            float [] toll_array=new float [roadSectionLst.size()];
            float [] winddirection_array=new float [roadSectionLst.size()];
            float [] windspeed_array=new float [roadSectionLst.size()];
            String [] beginningNode_array=new String[roadSectionLst.size()];
            String [] edingNode_array=new String[roadSectionLst.size()];
            String [] direction_array=new String[roadSectionLst.size()];
            Number [] projectID_array=new Number [roadSectionLst.size()];
            
            // Fill the Java arrays.
            for (int i=0; i< roadSectionLst.size(); i++) {
                road_id_array [i] = roadSectionLst.get(i).getRoadId();
                typology_array [i] = roadSectionLst.get(i).getTypology();
                toll_array [i] = roadSectionLst.get(i).getToll();
                winddirection_array [i] = roadSectionLst.get(i).getWindDirection();
                windspeed_array [i] = roadSectionLst.get(i).getWindSpeed();
                beginningNode_array [i] = roadSectionLst.get(i).getBeginningNode().getId();
                edingNode_array [i] = roadSectionLst.get(i).getEndingNode().getId();
                direction_array[i] = roadSectionLst.get(i).getDirection().toString();
                projectID_array[i] = id_Proj;                
            }
            
            // Cast the Java arrays into Oracle arrays
            ARRAY ora_road_id_array = new ARRAY (oracleVarcharCollection,   connection, road_id_array);
            ARRAY ora_typology_array    = new ARRAY (oracleVarcharCollection, connection, typology_array);
            ARRAY ora_toll_array  = new ARRAY (oracleNumberCollection, connection, toll_array);
            ARRAY ora_winddirection_array    = new ARRAY (oracleNumberCollection, connection, winddirection_array);
            ARRAY ora_windspeed_array  = new ARRAY (oracleNumberCollection, connection, windspeed_array);
            ARRAY ora_beginningNode_array   = new ARRAY (oracleVarcharCollection, connection, beginningNode_array);
            ARRAY ora_edingNode_array   = new ARRAY (oracleVarcharCollection, connection, edingNode_array);
            ARRAY ora_direction_array   = new ARRAY (oracleVarcharCollection, connection, direction_array);
            ARRAY ora_projectID_array  = new ARRAY (oracleNumberCollection, connection, projectID_array);
            
            // Bind the input arrays.
            
            callStmt.setObject(1, ora_road_id_array);
            callStmt.setObject(2, ora_typology_array);
            callStmt.setObject(3, ora_toll_array);
            callStmt.setObject(4, ora_winddirection_array);
            callStmt.setObject(5, ora_windspeed_array);
            callStmt.setObject(6, ora_beginningNode_array);
            callStmt.setObject(7, ora_edingNode_array);
            callStmt.setObject(8, ora_direction_array);
            callStmt.setObject(9, ora_projectID_array);
            
            // Bind the output array, this will contain any exception indexes.
            callStmt.registerOutParameter(10, OracleTypes.ARRAY, "INTEGER_T");
            
            callStmt.execute();            
            
            // Get any exceptions. Remember Oracle arrays index from 1,
            // so all indexes are +1 off.
            int[] errors = new int[100];
            ARRAY ora_errors = ((OracleCallableStatement)callStmt).getARRAY(10);
            // cast the oracle array back to a Java array
            // Remember, Oracle arrays are indexed from 1, while Java is indexed from zero
            // Any array indexes returned as failures from Oracle need to have 1 subtracted from them
            // to reference the correct Java array element!
            errors = ora_errors.getIntArray();
            System.out.println(errors.length);
           // System.out.println(errors[0]);
            
        connection.commit();
       }
       catch (Exception e){
           e.printStackTrace();
           connection.rollback();           
       }
        
             
        
    }
   
       //ADDSEGMENT - inserir segmentLst na BD 
        public void addSegment (List <Segment> segmentLst, String road_id, String endingNode, String beginingNode, int id_project) throws SQLException {
        
        connection.setAutoCommit(false);
        try {
            
            ArrayDescriptor oracleVarcharCollection = ArrayDescriptor.createDescriptor("VARCHAR_T",connection);
            ArrayDescriptor oracleIntegerCollection = ArrayDescriptor.createDescriptor("INTEGER_T",connection);
            ArrayDescriptor oracleNumberCollection = ArrayDescriptor.createDescriptor("NUMBER_T",connection);
            
            callStmt=connection.prepareCall("{ call insert_segment(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");
                      
            // JAVA arrays to hold the data.
            String [] segment_id_array=new String[segmentLst.size()];
            float [] initialHeight_array=new float[segmentLst.size()];
            float [] slope_array=new float [segmentLst.size()];
            float [] lenght_array=new float [segmentLst.size()];
            float [] maxVelocity_array=new float[segmentLst.size()];
            float [] minVelocity_array=new float[segmentLst.size()];
            int [] maxNumVehicles_array=new int[segmentLst.size()];            
            String [] roadID_array=new String [segmentLst.size()];
            String [] endingNode_array=new String[segmentLst.size()];
            String [] beginningNode_array=new String[segmentLst.size()];
            int [] project_id_array=new int [segmentLst.size()];
            
            // Fill the Java arrays.
            for (int i=0; i< segmentLst.size(); i++) {
                 segment_id_array[i] = segmentLst.get(i).getId();
                 initialHeight_array[i] = segmentLst.get(i).getInitialHeight();
                 slope_array[i] = segmentLst.get(i).getSlope();
                 lenght_array[i] = segmentLst.get(i).getLength();
                 maxVelocity_array[i] = segmentLst.get(i).getMaxVelocity();
                 minVelocity_array[i] = segmentLst.get(i).getMinVelocity();
                 maxNumVehicles_array[i] = segmentLst.get(i).getMaxNumberVehicles();
                 roadID_array[i] = road_id;
                 endingNode_array[i] = endingNode;
                 beginningNode_array[i] = beginingNode;
                 project_id_array[i]=id_project;
            }
            
            // Cast the Java arrays into Oracle arrays
            ARRAY ora_segment_id_array = new ARRAY (oracleVarcharCollection,   connection, segment_id_array);
            ARRAY ora_initialHeight_array   = new ARRAY (oracleNumberCollection, connection, initialHeight_array);
            ARRAY ora_slope_array  = new ARRAY (oracleNumberCollection, connection, slope_array);
            ARRAY ora_lenght_array    = new ARRAY (oracleNumberCollection, connection, lenght_array);
            ARRAY ora_maxVelocity_array  = new ARRAY (oracleNumberCollection, connection, maxVelocity_array);
            ARRAY ora_minVelocity_array   = new ARRAY (oracleNumberCollection, connection, minVelocity_array);
            ARRAY ora_maxNumVehicles_array   = new ARRAY (oracleNumberCollection, connection, maxNumVehicles_array);
            ARRAY ora_roadID_array   = new ARRAY (oracleVarcharCollection, connection, roadID_array);
            ARRAY ora_endingNode_array  = new ARRAY (oracleVarcharCollection, connection, endingNode_array);
            ARRAY ora_beginningNode_array  = new ARRAY (oracleVarcharCollection, connection, beginningNode_array);
            ARRAY ora_project_id_array  = new ARRAY (oracleNumberCollection, connection, project_id_array);
            
            // Bind the input arrays.            
            callStmt.setObject(1, ora_segment_id_array);
            callStmt.setObject(2, ora_initialHeight_array);
            callStmt.setObject(3, ora_slope_array);
            callStmt.setObject(4, ora_lenght_array);
            callStmt.setObject(5, ora_maxVelocity_array);
            callStmt.setObject(6, ora_minVelocity_array);
            callStmt.setObject(7, ora_maxNumVehicles_array);
            callStmt.setObject(8, ora_roadID_array);
            callStmt.setObject(9, ora_endingNode_array);
            callStmt.setObject(10, ora_beginningNode_array);
            callStmt.setObject(11, ora_project_id_array);
                    
            // Bind the output array, this will contain any exception indexes.
            callStmt.registerOutParameter(12, OracleTypes.ARRAY, "INTEGER_T");
            
            callStmt.execute();            
            
            // Get any exceptions. Remember Oracle arrays index from 1,
            // so all indexes are +1 off.
            int[] errors = new int[100];
            ARRAY ora_errors = ((OracleCallableStatement)callStmt).getARRAY(12);
            // cast the oracle array back to a Java array
            // Remember, Oracle arrays are indexed from 1, while Java is indexed from zero
            // Any array indexes returned as failures from Oracle need to have 1 subtracted from them
            // to reference the correct Java array element!
            errors = ora_errors.getIntArray();
            System.out.println(errors.length);
           // System.out.println(errors[0]);
            
        connection.commit();
       }
       catch (Exception e){
           e.printStackTrace();
           connection.rollback();           
       }            
    }
        
    
        
        //ADDVEHICLE - inserir vehicles na BD 
        public void addVehicle (List <Vehicle> vehicleLst, int id_Project) throws SQLException {
        
        connection.setAutoCommit(false);
        try {
            
            ArrayDescriptor oracleVarcharCollection = ArrayDescriptor.createDescriptor("VARCHAR_T",connection);
            ArrayDescriptor oracleIntegerCollection = ArrayDescriptor.createDescriptor("INTEGER_T",connection);
            ArrayDescriptor oracleNumberCollection = ArrayDescriptor.createDescriptor("NUMBER_T",connection);
            
            callStmt=connection.prepareCall("{ call insert_VEHICLE(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }");
                      
            // JAVA arrays to hold the data.
            String [] vehicle_name_array=new String[vehicleLst.size()];
            int [] id_project_array=new int[vehicleLst.size()];
            String [] description_array=new String [vehicleLst.size()];
            String [] type_array=new String [vehicleLst.size()];
            String [] motorization_array=new String[vehicleLst.size()];
            String [] fuel_array=new String[vehicleLst.size()];
            float [] mass_array=new float[vehicleLst.size()];            
            float [] load_array=new float [vehicleLst.size()];
            float [] drag_array=new float[vehicleLst.size()];
            float [] frontalArea_array=new float[vehicleLst.size()];
            float [] rrc_array=new float[vehicleLst.size()];
            float [] wheel_size_array=new float[vehicleLst.size()];
            
            // Fill the Java arrays.
            for (int i=0; i< vehicleLst.size(); i++) {
                 vehicle_name_array[i] = vehicleLst.get(i).getName();
                 id_project_array[i] = id_Project;
                 description_array[i] = vehicleLst.get(i).getDescription();
                 type_array[i] = vehicleLst.get(i).getType();
                 motorization_array[i] = vehicleLst.get(i).getMotorization();
                 fuel_array[i] = vehicleLst.get(i).getFuel();
                 mass_array[i] = vehicleLst.get(i).getMass();
                 load_array[i] = vehicleLst.get(i).getLoad();
                 drag_array[i] = vehicleLst.get(i).getDrag();
                 frontalArea_array[i] = vehicleLst.get(i).getFrontalArea();                 
                 rrc_array [i]= vehicleLst.get(i).getRrc();
                 wheel_size_array [i]=vehicleLst.get(i).getWheelsize();
            }
            
            // Cast the Java arrays into Oracle arrays
            ARRAY ora_vehicle_name_array = new ARRAY (oracleVarcharCollection,   connection, vehicle_name_array);
            ARRAY ora_id_project_array   = new ARRAY (oracleNumberCollection, connection, id_project_array);
            ARRAY ora_description_array  = new ARRAY (oracleVarcharCollection, connection, description_array);
            ARRAY ora_type_array   = new ARRAY (oracleVarcharCollection, connection, type_array);
            ARRAY ora_motorization_array  = new ARRAY (oracleVarcharCollection, connection, motorization_array);
            ARRAY ora_fuel_array   = new ARRAY (oracleVarcharCollection, connection, fuel_array);
            ARRAY ora_mass_array   = new ARRAY (oracleNumberCollection, connection, mass_array);
            ARRAY ora_load_array   = new ARRAY (oracleNumberCollection, connection, load_array);
            ARRAY ora_drag_array  = new ARRAY (oracleNumberCollection, connection, drag_array);
            ARRAY ora_frontalArea_array  = new ARRAY (oracleNumberCollection, connection, frontalArea_array);
            ARRAY ora_rrc_array  = new ARRAY (oracleNumberCollection, connection, rrc_array);
            ARRAY ora_wheel_size_array  = new ARRAY (oracleNumberCollection, connection, wheel_size_array);
            
            // Bind the input arrays.            
            callStmt.setObject(1, ora_vehicle_name_array);
            callStmt.setObject(2, ora_id_project_array);
            callStmt.setObject(3, ora_description_array);
            callStmt.setObject(4, ora_type_array);
            callStmt.setObject(5, ora_motorization_array);
            callStmt.setObject(6, ora_fuel_array);
            callStmt.setObject(7, ora_mass_array);
            callStmt.setObject(8, ora_load_array);
            callStmt.setObject(9, ora_drag_array);
            callStmt.setObject(10, ora_frontalArea_array);
            callStmt.setObject(11, ora_rrc_array);
            callStmt.setObject(12, ora_wheel_size_array);
                    
            // Bind the output array, this will contain any exception indexes.
            callStmt.registerOutParameter(13, OracleTypes.ARRAY, "INTEGER_T");
            callStmt.registerOutParameter(14, OracleTypes.ARRAY, "INTEGER_T");
            
            callStmt.execute();     
            
            int[] id_vehicles_array=new int [vehicleLst.size()];
            ARRAY ora_id_vehicles=((OracleCallableStatement)callStmt).getARRAY(13);
            
            id_vehicles_array=ora_id_vehicles.getIntArray();
            
                        // Get any exceptions. Remember Oracle arrays index from 1,
            // so all indexes are +1 off.
            int[] errors = new int[100];
            ARRAY ora_errors = ((OracleCallableStatement)callStmt).getARRAY(14);
            // cast the oracle array back to a Java array
            // Remember, Oracle arrays are indexed from 1, while Java is indexed from zero
            // Any array indexes returned as failures from Oracle need to have 1 subtracted from them
            // to reference the correct Java array element!
            errors = ora_errors.getIntArray();
            System.out.println(errors.length);
           // System.out.println(errors[0]);
            
           int i=0; 
           for(Vehicle v:vehicleLst){
               addVehlocityLimit(v.getVelocityLimitList().getVelocityLimitList(), id_vehicles_array[i]);
               addGear(v.getEnergy().getGearList().getGearList(), id_vehicles_array[i]);
               addThrolle(v.getEnergy().getThrottleLst().getThrottleLst(), id_vehicles_array[i]);
               for(Throttle t:v.getEnergy().getThrottleLst().getThrottleLst()){
                   addRegime(t.getRegimes(),id_vehicles_array[i],t.getId());
               }
               i++;
           }
            addEnergy(vehicleLst, id_vehicles_array);
           
            
        connection.commit();
       }
       catch (Exception e){
           e.printStackTrace();
           connection.rollback();           
       }            
    }    
        
    
        
            //VELOCITYLIMIT - inserir vehicles na BD 
        public void addVehlocityLimit (List <VelocityLimit> velocityLimitLst, int id_vehicle) throws SQLException {
        
        connection.setAutoCommit(false);
        try {
            
            ArrayDescriptor oracleVarcharCollection = ArrayDescriptor.createDescriptor("VARCHAR_T",connection);
            ArrayDescriptor oracleIntegerCollection = ArrayDescriptor.createDescriptor("INTEGER_T",connection);
            ArrayDescriptor oracleNumberCollection = ArrayDescriptor.createDescriptor("NUMBER_T",connection);
            
            callStmt=connection.prepareCall("{ call insert_VELOCITY_LIMIT(?, ?, ?, ?) }");
                      
            // JAVA arrays to hold the data.
            int [] vehicle_id_array=new int[velocityLimitLst.size()];
            String [] segment_type_array=new String[velocityLimitLst.size()];
            float [] limit_array=new float [velocityLimitLst.size()];
            
            
            // Fill the Java arrays.
            for (int i=0; i< velocityLimitLst.size(); i++) {
                 vehicle_id_array[i] = id_vehicle;
                 segment_type_array[i] = velocityLimitLst.get(i).getSegmentType();
                 limit_array[i] = velocityLimitLst.get(i).getLimit();
                 
            }
            
            // Cast the Java arrays into Oracle arrays
            ARRAY ora_vehicle_id_array = new ARRAY (oracleIntegerCollection,   connection, vehicle_id_array);
            ARRAY ora_segment_type_array   = new ARRAY (oracleVarcharCollection, connection, segment_type_array);
            ARRAY ora_limit_array  = new ARRAY (oracleNumberCollection, connection, limit_array);
            
            
            // Bind the input arrays.ora_vehicle_id_array             
            callStmt.setObject(1, ora_vehicle_id_array);
            callStmt.setObject(2, ora_segment_type_array);
            callStmt.setObject(3, ora_limit_array);           
            
                    
            // Bind the output array, this will contain any exception indexes.
            callStmt.registerOutParameter(4, OracleTypes.ARRAY, "INTEGER_T");
            
            callStmt.execute();      
                       
            // Get any exceptions. Remember Oracle arrays index from 1,
            // so all indexes are +1 off.
            int[] errors = new int[100];
            ARRAY ora_errors = ((OracleCallableStatement)callStmt).getARRAY(4);
            // cast the oracle array back to a Java array
            // Remember, Oracle arrays are indexed from 1, while Java is indexed from zero
            // Any array indexes returned as failures from Oracle need to have 1 subtracted from them
            // to reference the correct Java array element!
            errors = ora_errors.getIntArray();
            System.out.println(errors.length);
           // System.out.println(errors[0]);
            
        connection.commit();
       }
       catch (Exception e){
           e.printStackTrace();
           connection.rollback();           
       }            
    }      
   
   
        //ENERGY - inserir vehicles na BD 
        public void addEnergy (List <Vehicle> vehicleLst, int id_vehicle []) throws SQLException {
        
        connection.setAutoCommit(false);
        try {
            
            ArrayDescriptor oracleVarcharCollection = ArrayDescriptor.createDescriptor("VARCHAR_T",connection);
            ArrayDescriptor oracleIntegerCollection = ArrayDescriptor.createDescriptor("INTEGER_T",connection);
            ArrayDescriptor oracleNumberCollection = ArrayDescriptor.createDescriptor("NUMBER_T",connection);
            
            callStmt=connection.prepareCall("{ call insert_ENERGY(?, ?, ?, ?, ?, ?) }");
                      
            // JAVA arrays to hold the data.
            int [] vehicle_id_array=new int[vehicleLst.size()];
            int [] min_Rpm_array=new int[vehicleLst.size()];
            int [] max_rpm_array=new int [vehicleLst.size()];
            float [] final_drive_ratio_array=new float[vehicleLst.size()];
            float [] energy_regenerationratio_array=new float [vehicleLst.size()];
            
            
            // Fill the Java arrays.
            for (int i=0; i< vehicleLst.size(); i++) {
                 vehicle_id_array[i] = id_vehicle[i];
                 min_Rpm_array[i] = vehicleLst.get(i).getEnergy().getMinRpm();
                 max_rpm_array[i] = vehicleLst.get(i).getEnergy().getMaxRpm();
                 final_drive_ratio_array[i] = vehicleLst.get(i).getEnergy().getFinalDriveRatio();
                 energy_regenerationratio_array[i] = vehicleLst.get(i).getEnergy().getEnergy_regeneration_ratio();                 
            }
            
            // Cast the Java arrays into Oracle arrays
            ARRAY ora_vehicle_id_array = new ARRAY (oracleNumberCollection,   connection, vehicle_id_array);
            ARRAY ora_min_Rpm_array   = new ARRAY (oracleNumberCollection, connection, min_Rpm_array);
            ARRAY ora_max_rpm_array  = new ARRAY (oracleNumberCollection, connection, max_rpm_array);
            ARRAY ora_final_drive_ratio_array  = new ARRAY (oracleNumberCollection, connection, final_drive_ratio_array);
            ARRAY ora_energy_regenerationratio_array  = new ARRAY (oracleNumberCollection, connection, energy_regenerationratio_array);
            
            
            // Bind the input arrays.            
            callStmt.setObject(1, ora_vehicle_id_array);
            callStmt.setObject(2, ora_min_Rpm_array);
            callStmt.setObject(3, ora_max_rpm_array); 
            callStmt.setObject(4, ora_final_drive_ratio_array);
            callStmt.setObject(5, ora_energy_regenerationratio_array);  
            
                    
            // Bind the output array, this will contain any exception indexes.
            callStmt.registerOutParameter(6, OracleTypes.ARRAY, "INTEGER_T");
            
            callStmt.execute();            
            
            // Get any exceptions. Remember Oracle arrays index from 1,
            // so all indexes are +1 off.
            int[] errors = new int[100];
            ARRAY ora_errors = ((OracleCallableStatement)callStmt).getARRAY(6);
            // cast the oracle array back to a Java array
            // Remember, Oracle arrays are indexed from 1, while Java is indexed from zero
            // Any array indexes returned as failures from Oracle need to have 1 subtracted from them
            // to reference the correct Java array element!
            errors = ora_errors.getIntArray();
            System.out.println(errors.length);
           // System.out.println(errors[0]);
            
        connection.commit();
       }
       catch (Exception e){
           e.printStackTrace();
           connection.rollback();           
       }            
    } 
   
   
     
        
        //THROTTLE - inserir vehicles na BD 
        public void addThrolle (List <Throttle> throttleLst, int vehicle_id) throws SQLException {
        
        connection.setAutoCommit(false);
        try {
            
            ArrayDescriptor oracleVarcharCollection = ArrayDescriptor.createDescriptor("VARCHAR_T",connection);
            ArrayDescriptor oracleIntegerCollection = ArrayDescriptor.createDescriptor("INTEGER_T",connection);
            ArrayDescriptor oracleNumberCollection = ArrayDescriptor.createDescriptor("NUMBER_T",connection);
            
            callStmt=connection.prepareCall("{ call insert_THROTTLE(?, ?, ?) }");
                      
            // JAVA arrays to hold the data.
            
            String [] cod_throttle_array=new String[throttleLst.size()];
            int [] vehicle_id_array=new int [throttleLst.size()];
            
            
            // Fill the Java arrays.
            for (int i=0; i< throttleLst.size(); i++) {                 
                 cod_throttle_array[i] = throttleLst.get(i).getId();
                 vehicle_id_array[i] = vehicle_id;                 
            }
            
            // Cast the Java arrays into Oracle arrays
            ARRAY ora_cod_throttle_array = new ARRAY (oracleVarcharCollection,   connection, cod_throttle_array);
            ARRAY ora_vehicle_id_array  = new ARRAY (oracleNumberCollection, connection, vehicle_id_array);
            
            
            // Bind the input arrays.            
            callStmt.setObject(1, ora_cod_throttle_array);
            callStmt.setObject(2, ora_vehicle_id_array);               
                    
            // Bind the output array, this will contain any exception indexes.
            callStmt.registerOutParameter(3, OracleTypes.ARRAY, "INTEGER_T");
            
            callStmt.execute();            
            
            // Get any exceptions. Remember Oracle arrays index from 1,
            // so all indexes are +1 off.
            int[] errors = new int[100];
            ARRAY ora_errors = ((OracleCallableStatement)callStmt).getARRAY(3);
            // cast the oracle array back to a Java array
            // Remember, Oracle arrays are indexed from 1, while Java is indexed from zero
            // Any array indexes returned as failures from Oracle need to have 1 subtracted from them
            // to reference the correct Java array element!
            errors = ora_errors.getIntArray();
            System.out.println(errors.length);
           // System.out.println(errors[0]);
            
        connection.commit();
       }
       catch (Exception e){
           e.printStackTrace();
           connection.rollback();           
       }            
    }     
        
    
        
        //Gear - inserir gear na BD 
        public void addGear (List <Gear> gearLst, int vehicle_id) throws SQLException {
        
        connection.setAutoCommit(false);
        try {
            
            ArrayDescriptor oracleVarcharCollection = ArrayDescriptor.createDescriptor("VARCHAR_T",connection);
            ArrayDescriptor oracleIntegerCollection = ArrayDescriptor.createDescriptor("INTEGER_T",connection);
            ArrayDescriptor oracleNumberCollection = ArrayDescriptor.createDescriptor("NUMBER_T",connection);
            
            callStmt=connection.prepareCall("{ call insert_GEAR(?, ?, ?, ?) }");
                      
            // JAVA arrays to hold the data.
            
            String [] cod_gear_array=new String[gearLst.size()];
            int [] vehicle_id_array=new int[gearLst.size()];
            float [] ratio_array=new float[gearLst.size()];
            
            
            // Fill the Java arrays.
            for (int i=0; i< gearLst.size(); i++) {                 
                 cod_gear_array[i] = gearLst.get(i).getId();
                 vehicle_id_array[i] = vehicle_id; 
                 ratio_array[i] = gearLst.get(i).getRatio();
            }
            
            // Cast the Java arrays into Oracle arrays
            ARRAY ora_cod_gear_array = new ARRAY (oracleVarcharCollection,   connection, cod_gear_array);
            ARRAY ora_vehicle_id_array  = new ARRAY (oracleNumberCollection, connection, vehicle_id_array);
            ARRAY ora_ratio_array = new ARRAY (oracleNumberCollection, connection, ratio_array);
            
            // Bind the input arrays.            
            callStmt.setObject(1, ora_cod_gear_array);
            callStmt.setObject(2, ora_vehicle_id_array); 
            callStmt.setObject(3, ora_ratio_array); 
                    
            // Bind the output array, this will contain any exception indexes.
            callStmt.registerOutParameter(4, OracleTypes.ARRAY, "INTEGER_T");
            
            callStmt.execute();            
            
            // Get any exceptions. Remember Oracle arrays index from 1,
            // so all indexes are +1 off.
            int[] errors = new int[100];
            ARRAY ora_errors = ((OracleCallableStatement)callStmt).getARRAY(4);
            // cast the oracle array back to a Java array
            // Remember, Oracle arrays are indexed from 1, while Java is indexed from zero
            // Any array indexes returned as failures from Oracle need to have 1 subtracted from them
            // to reference the correct Java array element!
            errors = ora_errors.getIntArray();
            System.out.println(errors.length);
           // System.out.println(errors[0]);
            
        connection.commit();
       }
       catch (Exception e){
           e.printStackTrace();
           connection.rollback();           
       }            
    }     
        
        
        
     //REGIME - inserir gear na BD 
        public void addRegime (List <Regime> regimeLst, int vehicle_id, String cod_throttle) throws SQLException {
        
        connection.setAutoCommit(false);
        try {
            
            ArrayDescriptor oracleVarcharCollection = ArrayDescriptor.createDescriptor("VARCHAR_T",connection);
            ArrayDescriptor oracleIntegerCollection = ArrayDescriptor.createDescriptor("INTEGER_T",connection);
            ArrayDescriptor oracleNumberCollection = ArrayDescriptor.createDescriptor("NUMBER_T",connection);
            
            callStmt=connection.prepareCall("{ call insert_REGIME(?, ?, ?, ?, ?, ?, ?) }");
                      
            // JAVA arrays to hold the data.
            
            String [] cod_throttle_array=new String[regimeLst.size()];
            int [] vehicle_id_array=new int[regimeLst.size()];
            int [] torque_array=new int[regimeLst.size()];
            int [] rpm_low_array=new int[regimeLst.size()];
            int [] rpm_high_array=new int[regimeLst.size()];
            float [] rfc_array=new float[regimeLst.size()];
            
            
            // Fill the Java arrays.
            for (int i=0; i< regimeLst.size(); i++) {                 
                 cod_throttle_array[i] = cod_throttle;
                 vehicle_id_array[i] = vehicle_id; 
                 torque_array[i] = regimeLst.get(i).getTorque();
                 rpm_low_array[i] = regimeLst.get(i).getRpmLow(); 
                 rpm_high_array[i] = regimeLst.get(i).getRpmHigh();
                 rfc_array[i] = regimeLst.get(i).getSFC();
            }
            
            // Cast the Java arrays into Oracle arrays
            ARRAY ora_cod_throttle_array = new ARRAY (oracleVarcharCollection,   connection, cod_throttle_array);
            ARRAY ora_vehicle_id_array  = new ARRAY (oracleNumberCollection, connection, vehicle_id_array);
            ARRAY ora_torque_array = new ARRAY (oracleNumberCollection, connection, torque_array);
            ARRAY ora_rpm_low_array = new ARRAY (oracleNumberCollection,   connection, rpm_low_array);
            ARRAY ora_rpm_high_array  = new ARRAY (oracleNumberCollection, connection, rpm_high_array);
            ARRAY ora_rfc_array = new ARRAY (oracleNumberCollection, connection, rfc_array);
            
            // Bind the input arrays.            
            callStmt.setObject(1, ora_cod_throttle_array);
            callStmt.setObject(2, ora_vehicle_id_array); 
            callStmt.setObject(3, ora_torque_array);
            callStmt.setObject(4, ora_rpm_low_array);
            callStmt.setObject(5, ora_rpm_high_array); 
            callStmt.setObject(6, ora_rfc_array); 
                    
            // Bind the output array, this will contain any exception indexes.
            callStmt.registerOutParameter(7, OracleTypes.ARRAY, "INTEGER_T");
            
            callStmt.execute();            
            
            // Get any exceptions. Remember Oracle arrays index from 1,
            // so all indexes are +1 off.
            int[] errors = new int[100];
            ARRAY ora_errors = ((OracleCallableStatement)callStmt).getARRAY(7);
            // cast the oracle array back to a Java array
            // Remember, Oracle arrays are indexed from 1, while Java is indexed from zero
            // Any array indexes returned as failures from Oracle need to have 1 subtracted from them
            // to reference the correct Java array element!
            errors = ora_errors.getIntArray();
            System.out.println(errors.length);
           // System.out.println(errors[0]);
            
        connection.commit();
       }
       catch (Exception e){
           e.printStackTrace();
           connection.rollback();           
       }            
    }   
        
        
        
        
        
        //simulation - inserir gear na BD 
        public int addSimulation (Simulation s, int id_Project) throws SQLException {  
            
            
            int id_SIMULACAO=0;

            connection.setAutoCommit(false);
            try {
            callStmt=connection.prepareCall("{call ADD_SIMULATION (?, ?, ?, ?) }");

            // Especifica o parâmetro de entrada da função
            callStmt.registerOutParameter(1, OracleTypes.NUMBER); 
            callStmt.setInt(2,id_Project);
            callStmt.setString(3, s.getName());         
            callStmt.setString(4, s.getDescription());       

            // Executa a invocação da função/proc
            callStmt.execute();

            // Guarda o cursor retornado num objeto "ResultSet".
                id_SIMULACAO = callStmt.getInt(1);  

                addTrafficPattern(s.getLstTraffic().getLstTrafficPattern(), id_SIMULACAO);
            connection.commit();
           }
           catch (Exception e){
               connection.rollback();           
           }              
            return id_SIMULACAO;
    }
     
        
        
            
   
       
       public void addTrafficPattern (List <TrafficPattern> tpLst, int id_SIMULACAO) throws SQLException { 
           
           connection.setAutoCommit(false);
        try {
            
            ArrayDescriptor oracleVarcharCollection = ArrayDescriptor.createDescriptor("VARCHAR_T",connection);
            ArrayDescriptor oracleIntegerCollection = ArrayDescriptor.createDescriptor("INTEGER_T",connection);
            ArrayDescriptor oracleNumberCollection = ArrayDescriptor.createDescriptor("NUMBER_T",connection);
            
            callStmt=connection.prepareCall("{ call insert_TRAFFICP(?, ?, ?, ?, ?, ?) }");
                      
            // JAVA arrays to hold the data.
            
            int [] id_simulacao_array=new int[tpLst.size()];
            String [] begNode_array=new String[tpLst.size()];
            String [] endNode_array=new String[tpLst.size()];
            String [] vehicle_name_array=new String[tpLst.size()];
            float [] arrival_rate_array=new float[tpLst.size()];
            
            
            
            // Fill the Java arrays.
            for (int i=0; i< tpLst.size(); i++) {                 
                id_simulacao_array[i] = id_SIMULACAO;
                begNode_array [i] = tpLst.get(i).getBeg().getId(); 
                endNode_array[i] = tpLst.get(i).getEnd().getId();
                vehicle_name_array[i] = tpLst.get(i).getVehicle().getName(); 
                arrival_rate_array[i] =tpLst.get(i).getArrivalRate();
                 
            }
            
            // Cast the Java arrays into Oracle arrays
            ARRAY ora_id_simulacao_array = new ARRAY (oracleIntegerCollection,   connection, id_simulacao_array);
            ARRAY ora_begNode_array   = new ARRAY (oracleVarcharCollection, connection, begNode_array );
            ARRAY ora_endNode_array = new ARRAY (oracleVarcharCollection, connection, endNode_array);
            ARRAY ora_vehicle_name_array = new ARRAY (oracleVarcharCollection,   connection, vehicle_name_array);
            ARRAY ora_arrival_rate_array  = new ARRAY (oracleNumberCollection, connection,  arrival_rate_array);
           
            
            // Bind the input arrays.            
            callStmt.setObject(1, ora_id_simulacao_array);
            callStmt.setObject(2, ora_begNode_array); 
            callStmt.setObject(3, ora_endNode_array);
            callStmt.setObject(4, ora_vehicle_name_array);
            callStmt.setObject(5, ora_arrival_rate_array); 
                                
            // Bind the output array, this will contain any exception indexes.
            callStmt.registerOutParameter(6, OracleTypes.ARRAY, "INTEGER_T");
            
            callStmt.execute();            
            
            // Get any exceptions. Remember Oracle arrays index from 1,
            // so all indexes are +1 off.
            int[] errors = new int[100];
            ARRAY ora_errors = ((OracleCallableStatement)callStmt).getARRAY(6);
            // cast the oracle array back to a Java array
            // Remember, Oracle arrays are indexed from 1, while Java is indexed from zero
            // Any array indexes returned as failures from Oracle need to have 1 subtracted from them
            // to reference the correct Java array element!
            errors = ora_errors.getIntArray();
            System.out.println(errors.length);
           // System.out.println(errors[0]);
            
        connection.commit();
       }
       catch (Exception e){
           e.printStackTrace();
           connection.rollback();           
       }                  
       }
       
       
       
       
       
       
       
       
         public ArrayList<Simulation> getSimulation (Project p) throws SQLException { 
        //SIMULATION
           int id_project=p.getId();
            ArrayList<Simulation> arraySimulation =new ArrayList<>();    
            ArrayList<TrafficPattern> arrayTraffP=new ArrayList<>(); 

           callStmt = connection.prepareCall("{ ? = call getSIMULATION (?)}");
           callStmt.registerOutParameter(1, OracleTypes.CURSOR);
           callStmt.setInt(2, id_project); // simulation_id

           callStmt.execute();

           rSet = (ResultSet) callStmt.getObject(1);//tudo simulation
           System.out.println(" carregar simulations...");        

            while(rSet.next()){ //simulation
                Simulation s=new Simulation(rSet.getInt(1),rSet.getString(3), rSet.getString(4));                  
                
                //TrafficPattern   
               callStmt = connection.prepareCall("{ ? = call getTRAFFICP (?)}");
               callStmt.registerOutParameter(1, OracleTypes.CURSOR);
               callStmt.setInt(2, rSet.getInt(1)); //id_simulacao
               callStmt.execute();
            
            
                ResultSet rSet2 = (ResultSet) callStmt.getObject(1); //tudo trafficPattern         

                while(rSet2.next()){ 

                    Junction endNode=new Junction(rSet2.getString(4));
                    Junction begNode=new Junction (rSet2.getString(3));
                    Vehicle vehicle=p.getVehicleList().getVehicleByName(rSet2.getString(5));


                    TrafficPattern t= new TrafficPattern(begNode,
                                                        endNode,
                                                        vehicle,
                                                        rSet2.getInt(6));
                                       
                    arrayTraffP.add(t);
                    
                }if (arrayTraffP.isEmpty()) {
                    System.out.println("No trafficPattern found.");
                }else{
                    System.out.println("trafficPattern loaded succesfully.");
                }  
                
                
                 s.getLstTraffic().setLstTrafficPattern(arrayTraffP);
                 arraySimulation.add(s);

            }
            if (arraySimulation.isEmpty()) {
                System.out.println("No simulation found.");
            }else{
                System.out.println("Simulation loaded succesfully.");
            }   
            return arraySimulation;
        } 
         
         
         
         public void setProject (int id_Project, String name_Proj, String desc_Proj) throws SQLException { 
             
             callStmt=connection.prepareCall("{ call SETPROJECT(?, ?, ?) }");
             
             // Especifica o parâmetro de entrada da função
            callStmt.setInt(1, id_Project);
            callStmt.setString(2, name_Proj);  
            callStmt.setString(3, desc_Proj);                    

            // Executa a invocação da função/proc
            callStmt.execute();
             
         }
         
           public void setSimulation (int id_simulation, String name_simulation, String desc_simulation) throws SQLException { 
             
            callStmt=connection.prepareCall("{ call SETSIMULATION(?, ?, ?) }");
             
             // Especifica o parâmetro de entrada da função
            callStmt.setInt(1, id_simulation);
            callStmt.setString(2, name_simulation);  
            callStmt.setString(3, desc_simulation);                    

            // Executa a invocação da função/proc
            callStmt.execute();
             
         }
         
     
}
