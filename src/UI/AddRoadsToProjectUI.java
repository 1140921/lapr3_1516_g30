/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import controllers.AddRoadsToProjectController;
import controllers.ImportVehiclesController;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;
import model.Junction;
import model.Project;
import model.ProjectRegistry;
import model.RoadSection;
import model.Simulation;

/**
 *
 * @author 1140921
 */
public class AddRoadsToProjectUI extends javax.swing.JDialog {

    AddRoadsToProjectController m_controller;
    DefaultListModel<String> activeProjectModel = new DefaultListModel<>();
    ProjectRegistry projectRegistry;
    JFrame mainMenu;

    /**
     * Creates new form AddRoadsToProject
     */
    public AddRoadsToProjectUI(java.awt.Frame parent, boolean modal, ProjectRegistry projectRegistry) {
        super(parent, modal);
        mainMenu = (JFrame) parent;
        m_controller = new AddRoadsToProjectController(projectRegistry);
        if (m_controller.getActiveProject() != null) {
            activeProjectModel.addElement("Name : " + m_controller.getActiveProjectName() + "   " + "Description : " + m_controller.getActiveProjectDescription());
            activeProjectModel.addElement("Road Sections");
            activeProjectModel.addElement(m_controller.getActiveProjectRoads());
        }

        jList1 = new JList<>(activeProjectModel);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jButton1.setText("Import Roads");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jList1.setModel(activeProjectModel);
        jList1.setEnabled(false);
        jScrollPane1.setViewportView(jList1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(145, 145, 145)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1)
                .addGap(27, 27, 27))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        JFileChooser j = new JFileChooser();
        List<RoadSection> lstRoads = m_controller.getRoadSection();

        List<Junction> junctionLst = m_controller.getJunctions();

        FileNameExtensionFilter filter = new FileNameExtensionFilter("xml", "xml");
        j.setFileFilter(filter);
        setLocationRelativeTo(null);

        int res = j.showDialog(this, null);
        if (res == JFileChooser.CANCEL_OPTION) {
        } else if (res == JFileChooser.APPROVE_OPTION) {
            try {
                m_controller.importFile(j.getSelectedFile().getAbsolutePath());
                mainMenu.getContentPane().revalidate();
                mainMenu.validate();
                mainMenu.repaint();
                mainMenu.revalidate();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, e.getMessage(), "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }

        List<RoadSection> lst = new ArrayList<>();
        for (RoadSection list : lstRoads) {
            for (RoadSection x : m_controller.getRoadSection()) {
                if (list == x) {

                } else {
                    lst.add(x);
                }

            }

        }
        m_controller.setnewRoadsections(lst);

        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AddRoadsToProjectUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AddRoadsToProjectUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AddRoadsToProjectUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AddRoadsToProjectUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            ProjectRegistry projectRegistry;

            public void run() {
                AddRoadsToProjectUI dialog = new AddRoadsToProjectUI(new javax.swing.JFrame(), true, projectRegistry);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JList jList1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
