
package model;

public class Gear {

    private String id;
    private float ratio;
    
    public Gear(){
    }
    
    public Gear(String id,float ratio){
        this.id=id;
        this.ratio=ratio;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the ratio
     */
    public float getRatio() {
        return ratio;
    }

    /**
     * @param ratio the ratio to set
     */
    public void setRatio(float ratio) {
        this.ratio = ratio;
    }
    
    public String toString(){
        return "Gear - id: "+this.id+" ratio: "+this.ratio;
    }
    
}
