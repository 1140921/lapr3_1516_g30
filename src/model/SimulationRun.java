package model;

import java.util.ArrayList;
import java.util.List;

public class SimulationRun {

    private String runName;
    private int duration;
    private double timeStep;
    private BestPath bestPath;
    private SimulationResults simulationResults;
    private double clock = 0;
    private Project project;
    private TrafficList tl;
    private List<Vehicle> garbage;
    private Behavior behavior;

    public SimulationRun(String runName, int duration, double timeStep, BestPath bestPath) {
        this.runName = runName;
        this.duration = duration;
        this.timeStep = timeStep;
        this.bestPath = bestPath;
    }

    public SimulationRun(String runName, int duration, double timeStep, BestPath bestPath, Project project,
            TrafficList tl) {
        this.runName = runName;
        this.duration = duration;
        this.timeStep = timeStep;
        this.bestPath = bestPath;
        this.project = project;
        this.tl = tl;
    }

    /**
     * @return the runName
     */
    public String getRunName() {
        return runName;
    }

    /**
     * @param runName the runName to set
     */
    public void setRunName(String runName) {
        this.runName = runName;
    }

    /**
     * @return the duration
     */
    public int getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * @return the timeStep
     */
    public double getTimeStep() {
        return timeStep;
    }

    /**
     * @param timeStep the timeStep to set
     */
    public void setTimeStep(double timeStep) {
        this.timeStep = timeStep;
    }

    /**
     * @return the bestPath
     */
    public BestPath getBestPath() {
        return bestPath;
    }

    /**
     * @param bestPath the bestPath to set
     */
    public void setBestPath(BestPath bestPath) {
        this.bestPath = bestPath;
    }

    /**
     * @return the simulationResults
     */
    public SimulationResults getSimulationResults() {
        return simulationResults;
    }

    /**
     * @param simulationResults the simulationResults to set
     */
    public void setSimulationResults(SimulationResults simulationResults) {
        this.simulationResults = simulationResults;
    }

    private List<List<RoadSection>> begin() {
        List<List<RoadSection>> paths = new ArrayList<>();
        for (TrafficPattern t : tl.getLstTrafficPattern()) {
            //paths.add(bestPath.getPath(project.getRoadNetwork(), t.getBeg(), t.getEnd(), t.getVehicle()).getSections());
        }
        return paths;
    }

    public void run() {
        List<List<RoadSection>> paths = begin();
        int cont=0;
        while (clock < duration) {
            for (List<RoadSection> path : paths) {
                for (RoadSection rs : path) {
                    
                }
            }
 
            clock = clock + timeStep;
        }
    }

    public float time(float velocity, float distance) {
        return distance / velocity;
    }

    private static float velocity(Vehicle vehicle, String type, Segment seg) {
        Energy eAux=vehicle.getEnergy();
        float velocity = Calculus.carVelocity(eAux.getMaxRpm(),eAux.getFinalDriveRatio(),
                eAux.getGearList().last().getRatio());
               
        for (VelocityLimit vl : vehicle.getVelocityLimitList().getVelocityLimitList()) {
            if (type.equalsIgnoreCase(vl.getSegmentType()) && velocity>vl.getLimit()) {
                velocity= vl.getLimit();
            }
        }
        if (seg.getMaxVelocity() < velocity) {
            velocity = seg.getMaxVelocity();
        }
        return velocity;
    }

}
