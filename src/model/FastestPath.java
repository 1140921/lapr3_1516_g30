package model;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class FastestPath extends BestPath {

    @Override
    public NetworkAnalisysResults getPath(Graph<Junction, List<RoadSection>> g, Junction orig, Junction dest, Vehicle vehicle) {
        Deque<Junction> shortPath = new LinkedList<>();
        for (Edge<Junction, List<RoadSection>> e : g.edges()) {
            float menor = 0;
            float aux;
            for (RoadSection roads : e.getElement()) {
                aux = timeRoadSection(roads, vehicle);
                if (menor == 0 || aux < menor) {
                    menor = aux;
                }
            }
            e.setWeight(menor);
        }

        GraphAlgorithms.shortestPath(g, orig, dest, shortPath);
        
        List<RoadSection> pathEdge = new ArrayList<>();
        Junction shortPathArray[] = new Junction[shortPath.size()];
        shortPathArray = shortPath.toArray(shortPathArray);

        int j = 0;
        float travelTime = 0;
        float energy=0;
        float tolls=0;
        for (int i = 1; i < shortPathArray.length; i++) {
            Vertex<Junction, List<RoadSection>> vOrig = g.getVertex(shortPathArray[j]);
            Vertex<Junction, List<RoadSection>> vDest = g.getVertex(shortPathArray[i]);
            for (RoadSection roads : g.getEdge(vOrig, vDest).getElement()) {
                float time = timeRoadSection(roads, vehicle);
                if (time == g.getEdge(vOrig, vDest).getWeight()) {
                    time = timeRoadSection(roads, vehicle);
                    travelTime = travelTime + time;
                    pathEdge.add(roads);
                    energy=energy+energy(vehicle, roads);
                    tolls=tolls+roads.getToll();
                }
            }
            j++;
        }
        return new NetworkAnalisysResults(pathEdge, travelTime, energy, tolls);
    }

    private static float energy(Vehicle v, RoadSection rs) {
        float energyExpended = 0;
 
            for (Segment s : rs.getSegmentList().getSegmentList()) {
                float velocity = velocity(v, rs.getTypology(), s);
                energyExpended = energyExpended + Calculus.work(v.getMass()+v.getLoad(), 
                velocity,v.getRrc(),rs.getWindSpeed(),rs.getWindDirection(),s.getSlope(),s.getLength());
            }
        
        return energyExpended;
    }
       
    @Override
    public String toString() {
        return "Fastest path";
    }
}
