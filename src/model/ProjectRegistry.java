package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProjectRegistry {

    private List<Project> projectList;

    private Project activeProject;

    public ProjectRegistry() {
        projectList = new ArrayList<>();
    }

    public boolean modifiyData(String name, String desc) {
        if (!validProject(name, desc)) {
            return false;
        } else {
            activeProject.setName(name);
            activeProject.setDescription(desc);
        }
        return true;

    }

    public boolean validProject(String name, String desc) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Invalid Name");
        } else if (desc == null || desc.isEmpty()) {
            throw new IllegalArgumentException("Invalid Description");
        } else {

            for (Project pr : projectList) {
                if (pr.getName().equalsIgnoreCase(name)) {
                    return false;
                }
            }
            return true;
        }
    }

    public ProjectRegistry(List<Project> projectList) {
        this.projectList = new ArrayList<>(projectList);
    }

    public List<Project> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<Project> projectList) {
        this.projectList.clear();
        this.projectList.addAll(projectList);
    }

    public Project getProject(int indice) {
        return this.projectList.get(indice);
    }

    public boolean addProject(Project project) {
        return this.projectList.contains(project)
                ? false
                : this.projectList.add(project);
    }

    public int addProjectList(ProjectRegistry projectList) {
        int totalProjectAdded = 0;
        for (Project project : projectList.getProjectList()) {
            boolean projectAdded = this.addProject(project);
            if (projectAdded) {
                totalProjectAdded++;
            }
        }
        return totalProjectAdded;
    }

    public boolean removeProject(Project project) {
        return this.projectList.remove(project);
    }

    public Project removeProject(int indice) {
        return this.projectList.remove(indice);
    }

    public boolean removeAll(ProjectRegistry projectList) {
        return this.projectList.removeAll(projectList.getProjectList());
    }

    public void clean() {
        this.projectList.clear();
    }

    public int size() {
        return this.projectList.size();
    }

    public boolean isEmpty() {
        return this.projectList.isEmpty();
    }

    public Project[] getArray() {
        return this.projectList.toArray(
                new Project[this.projectList.size()]);
    }

//    @Override
//    public String toString() {
//        Collections.sort(this.listaTelefonica);
//        StringBuilder s = new StringBuilder();
//        for (Telefone telefone : this.listaTelefonica) {
//            s.append(telefone);
//            s.append("\n");
//        }
//        return String.format("Lista Telefónica%n%s", s.toString());
//    }
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }
        ProjectRegistry otherProjectList = (ProjectRegistry) outroObjeto;
        return this.projectList.equals(otherProjectList.getProjectList());
    }

    /**
     * @return the activeProject
     */
    public Project getActiveProject() {
        return activeProject;
    }

    /**
     * @param activeProject the activeProject to set
     */
    public void setActiveProject(Project activeProject) {
        this.activeProject = activeProject;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Project List: \n");
        for (Project p : projectList) {
            sb.append(p);
            sb.append("\n");
        }
        return sb.toString();
    }
}
