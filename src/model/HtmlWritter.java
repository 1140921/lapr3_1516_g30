/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.Formatter;

/**
 *
 * @author 1140921
 */
public abstract class HtmlWritter implements ExportData{

    /**
     * Iniciar Pa¡gina HTML
     *
     * @param pag - ficheiro pÃ¡gina
     * @param ProjectRegistry - projectRegistry
     */
    Formatter pag;
    private Project activeProject;
    private Simulation simulation;
    public void iniciarPagina(String path, ProjectRegistry projectRegistry) throws FileNotFoundException {
        new Formatter(new File(path));
        activeProject=projectRegistry.getActiveProject();
        simulation= activeProject.getSimulations().getActiveSimulation();
        pag.format("<!DOCTYPE html>%n");
        pag.format("<html>%n");
        pag.format("<head>%n");
        pag.format("<meta charset='utf-8'>%n");
        pag.format("<title>%s</title>%n","Project Name", activeProject.getName(),"Simulation Name",simulation.getName());
        pag.format("</head>%n");
        pag.format("<body>");
        
        
        
        
        closeFile();
    }

    /**
     * Fechar a estrutura da pÃ¡gina
     *
     * @param pag - ficheiro pÃ¡gina
     */
    public  void closeFile() {
        pag.format("</body>%n</html>");
    }

 

    /**
     * Cria na pÃ¡gina um cabeÃ§alho (header) de tamanho n
     *
     * @param pag - ficheiro pÃ¡gina
     * @param n - tamanho do cabeÃ§alho
     * @param conteudo - conteÃºdo do cabeÃ§alho
     */
    public  void cabecalho( int n, String conteudo) {
        pag.format("<h%d>%s</h%d>%n", n, conteudo, n);
    }

    /**
     * Cria na pÃ¡gina um parÃ¡grafo
     *
     * @param pag - ficheiro pÃ¡gina
     * @param conteudo - conteÃºdo do parÃ¡grafo
     */
    public  void paragrafo( String conteudo) {
        pag.format("<p>%s</p>%n", conteudo);
    }

    /**
     * Iniciar a criaÃ§Ã£o de uma tabela
     *
     * @param pag
     */
    public  void iniciarTabela() {
        pag.format("<table>%n");
    }

   

    /**
     * Insereir na tabela uma linha de cabeÃ§alhos
     *
  
     * @param info
     */
    public  void linhaCabecalhoTabela( SimulationResults info) {
        pag.format("<tr>");
        
//        for (SimulationResults s : SimulationResults.) {
//            pag.format("<th>%s</th>", info[i]);
//        }
        pag.format("</tr>%n");
    }

  

}
