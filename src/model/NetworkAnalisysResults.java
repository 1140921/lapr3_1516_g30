
package model;

import java.util.List;


public class NetworkAnalisysResults {
    private List<RoadSection> sections;
    private float travelingTime;
    private float energyConsumption;
    private float tollCosts;
        
    public NetworkAnalisysResults(List<RoadSection>sections,float travelingTime,
            float energyConsumption,float tollCosts){
        this.sections=sections;
        this.travelingTime=travelingTime;
        this.tollCosts=tollCosts;
        this.energyConsumption=energyConsumption;
    }

    /**
     * @return the sections
     */
    public List<RoadSection> getSections() {
        return sections;
    }

    /**
     * @param sections the sections to set
     */
    public void setSections(List<RoadSection> sections) {
        this.sections = sections;
    }

    /**
     * @return the travelingTime
     */
    public float getTravelingTime() {
        return travelingTime;
    }

    /**
     * @param travelingTime the travelingTime to set
     */
    public void setTravelingTime(float travelingTime) {
        this.travelingTime = travelingTime;
    }

    /**
     * @return the energyConsumption
     */
    public float getEnergyConsumption() {
        return energyConsumption;
    }

    /**
     * @param energyConsumption the energyConsumption to set
     */
    public void setEnergyConsumption(float energyConsumption) {
        this.energyConsumption = energyConsumption;
    }

    /**
     * @return the tollCosts
     */
    public float getTollCosts() {
        return tollCosts;
    }

    /**
     * @param tollCosts the tollCosts to set
     */
    public void setTollCosts(float tollCosts) {
        this.tollCosts = tollCosts;
    }
    public String sectionsStr(){
        StringBuilder sb = new StringBuilder();
        for(RoadSection rs:sections){
            sb.append(rs.getRoadId());
            sb.append("\n");
        }
    return sb.toString();
    }
    
    
    @Override
    public String toString(){
        return "--> List of Sections: "+sectionsStr()+"Traveling Time: "+this.getTravelingTime()
                +"\nEnergy Consumption: "+this.getEnergyConsumption()+"\nToll Costs: "+this.getTollCosts();
    }

}
