
package model;


public class Energy {

    private int minRpm;
    private int maxRpm;
    private float finalDriveRatio;
    private float energy_regeneration_ratio;
    private GearList gearList;
    private ThrottleList throttleLst;
    
    public Energy(){
    }
    
    public Energy(int minRpm,int maxRpm,float finalDriveRatio){
        this.minRpm=minRpm;
        this.maxRpm=maxRpm;
        this.finalDriveRatio=finalDriveRatio;
        this.gearList=new GearList();
        this.throttleLst=new ThrottleList();
    }
    
     public Energy(int minRpm,int maxRpm,float finalDriveRatio, float energy_regeneration_ratio){
        this.minRpm=minRpm;
        this.maxRpm=maxRpm;
        this.finalDriveRatio=finalDriveRatio;
        this.energy_regeneration_ratio=energy_regeneration_ratio;
        this.gearList=new GearList();
        this.throttleLst=new ThrottleList();
    }


    /**
     * @return the minRpm
     */
    public int getMinRpm() {
        return minRpm;
    }

    /**
     * @param minRpm the minRpm to set
     */
    public void setMinRpm(int minRpm) {
        this.minRpm = minRpm;
    }

    /**
     * @return the maxRpm
     */
    public int getMaxRpm() {
        return maxRpm;
    }

    /**
     * @param maxRpm the maxRpm to set
     */
    public void setMaxRpm(int maxRpm) {
        this.maxRpm = maxRpm;
    }

    /**
     * @return the finalDriveRatio
     */
    public float getFinalDriveRatio() {
        return finalDriveRatio;
    }

    /**
     * @param finalDriveRatio the finalDriveRatio to set
     */
    public void setFinalDriveRatio(float finalDriveRatio) {
        this.finalDriveRatio = finalDriveRatio;
    }

    /**
     * @return the gearList
     */
    public GearList getGearList() {
        return gearList;
    }

    /**
     * @param gearList the gearList to set
     */
    public void setGearList(GearList gearList) {
        this.gearList = gearList;
    }

    /**
     * @return the throttleLst
     */
    public ThrottleList getThrottleLst() {
        return throttleLst;
    }

    /**
     * @param throttleLst the throttleLst to set
     */
    public void setThrottleLst(ThrottleList throttleLst) {
        this.throttleLst = throttleLst;
    }

    @Override
    public String toString(){
       return "Energy - Min RPM: "+minRpm+" Max RPM: "+maxRpm+" Final Drive Ratio: "+finalDriveRatio
               +"\n"+gearList+" "+throttleLst;
    }

    /**
     * @return the energy_regeneration_ratio
     */
    public float getEnergy_regeneration_ratio() {
        return energy_regeneration_ratio;
    }

    /**
     * @param energy_regeneration_ratio the energy_regeneration_ratio to set
     */
    public void setEnergy_regeneration_ratio(float energy_regeneration_ratio) {
        this.energy_regeneration_ratio = energy_regeneration_ratio;
    }


}
