/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author 1140921
 */

public class Behavior {
    
    private float intantIn;
    private float intantOut;
    private float energySpent;
    
    public Behavior(float intantIn, float intantOut, float energySpent){
        setIntantIn(intantIn);
        setIntantOut(intantOut);
        setEnergySpent(energySpent);
    }

    /**
     * @return the intantIn
     */
    public float getIntantIn() {
        return intantIn;
    }

    /**
     * @param intantIn the intantIn to set
     */
    public void setIntantIn(float intantIn) {
        this.intantIn = intantIn;
    }

    /**
     * @return the intantOut
     */
    public float getIntantOut() {
        return intantOut;
    }

    /**
     * @param intantOut the intantOut to set
     */
    public void setIntantOut(float intantOut) {
        this.intantOut = intantOut;
    }

    /**
     * @return the energySpent
     */
    public float getEnergySpent() {
        return energySpent;
    }

    /**
     * @param energySpent the energySpent to set
     */
    public void setEnergySpent(float energySpent) {
        this.energySpent = energySpent;
    }
    
    
}
