package model;

import java.util.ArrayList;
import java.util.List;

public class TrafficList {

    private List<TrafficPattern> lstTraffic;

    public TrafficList() {
        this.lstTraffic = new ArrayList<>();
    }

    /**
     * @return the lstTrafficPattern
     */
    public List<TrafficPattern> getLstTrafficPattern() {
        return lstTraffic;
    }

    /**
     * @param lstTrafficPattern the lstTrafficPattern to set
     */
    public void setLstTrafficPattern(List<TrafficPattern> lstTrafficPattern) {
        this.lstTraffic = lstTrafficPattern;
    }

    public boolean addTrafficPattern(TrafficPattern tp) {
        return this.lstTraffic.contains(tp)
                ? false
                : this.lstTraffic.add(tp);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Traffic List: \n");
        for (TrafficPattern t : lstTraffic) {
            sb.append(t);
            sb.append("\n");
        }
        return sb.toString();
    }

}
