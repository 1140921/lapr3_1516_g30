package model;

public class Junction {

    private String id;

    public Junction(String id) {
        this.id = id;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    
      @Override
    public String toString() {
        return "Junction id: "+this.id;   
    }
    
      @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        Junction otherJuction = (Junction) o;

        return this.id.equals(otherJuction.getId());
    }
    
}
