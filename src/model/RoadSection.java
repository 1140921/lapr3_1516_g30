package model;

public class RoadSection {

    private String roadId;
    private Junction beginningNode;
    private Junction endingNode;
    private String typology;
    private Direction direction;
    private float toll;
    private float windDirection;
    private float windSpeed;
    private SegmentList segmentList;

    public RoadSection(String roadId, Junction beginningNode, Junction endingNode, String typology, 
            Direction direction, float toll, float windDirection, float windSpeed) {
        this.roadId = roadId;
        this.beginningNode = beginningNode;
        this.endingNode = endingNode;
        this.typology = typology;
        this.direction = direction;
        this.toll = toll;
        this.windDirection = windDirection;
        this.windSpeed = windSpeed;
        this.segmentList = new SegmentList();
    }

    /**
     * @return the beginningNode
     */
    public Junction getBeginningNode() {
        return beginningNode;
    }

    /**
     * @param beginningNode the beginningNode to set
     */
    public void setBeginningNode(Junction beginningNode) {
        this.beginningNode = beginningNode;
    }

    /**
     * @return the endingNode
     */
    public Junction getEndingNode() {
        return endingNode;
    }

    /**
     * @param endingNode the endingNode to set
     */
    public void setEndingNode(Junction endingNode) {
        this.endingNode = endingNode;
    }

    /**
     * @return the typology
     */
    public String getTypology() {
        return typology;
    }

    /**
     * @param typology the typology to set
     */
    public void setTypology(String typology) {
        this.typology = typology;
    }

    /**
     * @return the direction
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * @param direction the direction to set
     */
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    /**
     * @return the toll
     */
    public float getToll() {
        return toll;
    }

    /**
     * @param toll the toll to set
     */
    public void setToll(float toll) {
        this.toll = toll;
    }

    /**
     * @return the windDirection
     */
    public float getWindDirection() {
        return windDirection;
    }

    /**
     * @param windDirection the windDirection to set
     */
    public void setWindDirection(float windDirection) {
        this.windDirection = windDirection;
    }

    /**
     * @return the windSpeed
     */
    public float getWindSpeed() {
        return windSpeed;
    }

    /**
     * @param windSpeed the windSpeed to set
     */
    public void setWindSpeed(float windSpeed) {
        this.windSpeed = windSpeed;
    }

    /**
     * @return the segmentList
     */
    public SegmentList getSegmentList() {
        return segmentList;
    }

    /**
     * @return the roadId
     */
    public String getRoadId() {
        return roadId;
    }

    /**
     * @param roadId the roadId to set
     */
    public void setRoadId(String roadId) {
        this.roadId = roadId;
    }

    @Override
    public String toString() {
        return stringBuilder();
//                "Road ID:" + roadId + "\nBeg Node: " + beginningNode + "\nEnd Node: " + endingNode
//                + "\nTypology: " + typology + "\nDirection: " + direction.toString() + "\nToll: " + toll
//                + "\nWind Direction: " + windDirection + "\nWind Speed: " + windSpeed
//                + segmentList.toString();
    }
    public String stringBuilder() {
        StringBuilder result = new StringBuilder();
        result.append("  Road Id :"+roadId);
        result.append("\n");
        result.append("  Beginig Node :"+beginningNode);
        result.append("\n");
        result.append("  Ending Node :"+endingNode);
        result.append("\n");
        result.append("  Direction :"+typology);
        result.append("\n");
        result.append("  Toll :"+toll);
        result.append("\n");
        result.append("  Wind Direction :"+windDirection);
        result.append("\n");
        result.append("  Wind Speed :"+windSpeed);
        result.append("\n");
        return result.toString();
    }

    /**
     * @param segmentList the segmentList to set
     */
    public void setSegmentList(SegmentList segmentList) {
        this.segmentList = segmentList;
    }
    
      @Override
    public boolean equals(Object otherRoadSection) {
        if (this == otherRoadSection) {
            return true;
        }
        if (otherRoadSection == null || this.getClass() != otherRoadSection.getClass()) {
            return false;
        }
        RoadSection roadSection = (RoadSection) otherRoadSection;

        return this.roadId.equals(roadSection.getRoadId())&&this.beginningNode.equals(roadSection.getBeginningNode())
                &&this.endingNode.equals(roadSection.getEndingNode())&& this.typology.equals(roadSection.getTypology());
    }
}
