package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ReadVehiclesXML implements InputData {

    private VehiclesList vehicleList1;
    private Vehicle vehicle;
    private Energy energy;
    private GearList gearList;
    private Gear gear;
    private VelocityLimitList velocityLimitList;
    private VelocityLimit velocityLimity;
    private Throttle throttle;

    @Override
    public void read(Project project, String path) {
        this.vehicleList1 = project.getVehicleList();
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            DefaultHandler handler = new DefaultHandler() {
                boolean vehicleTag = false;
                boolean typeTag = false;
                boolean motorizationTag = false;
                boolean fuelTag = false;
                boolean massTag = false;
                boolean loadTag = false;
                boolean dragTag = false;
                boolean rrcTag = false;
                boolean wheelSizeTag = false;
                boolean velocityLimitListTag = false;
                boolean velocityLimitTag = false;
                boolean segmentTypeTag = false;
                boolean limitTag = false;
                boolean energyTag = false;
                boolean torqueTag = false;
                boolean rpmTag = false;
                boolean consumptionTag = false;
                boolean minRpmTag = false;
                boolean maxRpmTag = false;
                boolean finalDriveRatioTag = false;
                boolean gearListTag = false;
                boolean gearTag = false;
                boolean ratioTag = false;
                boolean throttleTag = false;
                boolean throttle_listTag = false;
                boolean regimeTag = false;
                boolean sfcTag = false;
                boolean rpm_lowTag = false;
                boolean rpm_highTag = false;
                boolean frontal_areaTag = false;
                boolean energy_regeneration_ratioTag = false;

                String nameVehicle = "";
                String descriptionVehicle = "";
                String typeVehicle = "";
                String motorizationVehicle = "";
                String fuelVehicle = "";
                float massVehicle;
                float loadVehicle;
                float dragVehicle;
                float rrcVehicle;
                float wheelSizeVehicle;
                String segmentTypeVelocity = "";
                float limitVelocity;
                int torqueEnergy;
                float rpmEnergy;
//                float consumptionEnergy;
                int minRpmEnergy;
                int maxRpmEnergy;
                float finalDriveRatioEnergy;
                String id;
                float ratioEnergy;
                float SFC = 0;
                int rpm_low;
                int rpm_high;
                String throttle_id = "";
                float frontal_area;
                float energy_regenerationRatio = 0;

                public void startElement(String uri, String localName,
                        String qName, Attributes attributes)
                        throws SAXException {

                    if (qName.equalsIgnoreCase("vehicle")) {
                        String name = attributes.getValue("name");
                        String description = attributes.getValue("description");
                        vehicle = new Vehicle();
                        vehicle.setName(name);
                        vehicle.setDescription(description);

                        vehicleTag = true;
                    }

                    if (qName.equalsIgnoreCase("type")) {
                        typeTag = true;
                    }
                    if (qName.equalsIgnoreCase("motorization")) {
                        motorizationTag = true;
                    }
                    if (qName.equalsIgnoreCase("fuel")) {
                        fuelTag = true;
                    }
                    if (qName.equalsIgnoreCase("mass")) {
                        massTag = true;
                    }
                    if (qName.equalsIgnoreCase("load")) {
                        loadTag = true;
                    }
                    if (qName.equalsIgnoreCase("drag")) {
                        dragTag = true;
                    }
                    if (qName.equalsIgnoreCase("rrc")) {
                        rrcTag = true;
                    }
                    if (qName.equalsIgnoreCase("wheel_size")) {
                        wheelSizeTag = true;
                    }
                    if (qName.equalsIgnoreCase("velocity_limit_list")) {
                        velocityLimitList = new VelocityLimitList();
                        velocityLimitListTag = true;
                    }
                    if (qName.equalsIgnoreCase("velocity_limit")) {
                        velocityLimity = new VelocityLimit();
                        velocityLimitTag = true;
                    }
                    if (qName.equalsIgnoreCase("segment_type")) {
                        segmentTypeTag = true;
                    }
                    if (qName.equalsIgnoreCase("limit")) {
                        limitTag = true;
                    }
                    if (qName.equalsIgnoreCase("energy")) {
                        energy = new Energy();
                        energyTag = true;
                    }
                    if (qName.equalsIgnoreCase("torque")) {
                        torqueTag = true;
                    }
                    if (qName.equalsIgnoreCase("rpm")) {
                        rpmTag = true;
                    }
                    if (qName.equalsIgnoreCase("consumption")) {
                        consumptionTag = true;
                    }
                    if (qName.equalsIgnoreCase("min_rpm")) {
                        minRpmTag = true;
                    }
                    if (qName.equalsIgnoreCase("max_rpm")) {
                        maxRpmTag = true;
                    }
                    if (qName.equalsIgnoreCase("final_drive_ratio")) {
                        finalDriveRatioTag = true;
                    }
                    if (qName.equalsIgnoreCase("gear_list")) {
                        gearList = new GearList();

                        gearListTag = true;
                    }
                    if (qName.equalsIgnoreCase("gear")) {
                        gear = new Gear();
                        gear.setId(attributes.getValue("id"));
                        //  System.out.println("gear id: " + gear.getId());
                        gearTag = true;
                    }
                    if (qName.equalsIgnoreCase("ratio")) {
                        ratioTag = true;
                    }
                    if (qName.equalsIgnoreCase("throttle")) {

                        //   System.out.println("-->" + attributes.getValue("id"));
                        throttle = new Throttle(attributes.getValue("id"));
                        throttle.setId(attributes.getValue("id"));
                        throttleTag = true;
                    }
                    if (qName.equalsIgnoreCase("regime")) {

                        regimeTag = true;
                    }
                    if (qName.equalsIgnoreCase("SFC")) {

                        sfcTag = true;
                    }
                    if (qName.equalsIgnoreCase("rpm_low")) {

                        rpm_lowTag = true;
                    }
                    if (qName.equalsIgnoreCase("rpm_high")) {

                        rpm_highTag = true;
                    }
                    if (qName.equalsIgnoreCase("frontal_area")) {

                        frontal_areaTag = true;
                    }
                    if (qName.equalsIgnoreCase("energy_regeneration_ratio")) {

                        energy_regeneration_ratioTag = true;
                    }
                }

                public void endElement(String uri, String localName,
                        String qName) throws SAXException {

                    if (qName.equalsIgnoreCase("vehicle")) {
                        vehicle.setVelocityLimitList(velocityLimitList);

                        if (project.getVehicleList().getVehicleList().size() == 2) {
                        
                            String name = vehicle.getName();
                            if (!project.getVehicleList().getVehicleList().contains(vehicle)&&project.getVehicleList().getVehicleByName(vehicle.getName())!=null) {
                                int validateId = project.getVehicleList().getVehicleList().size()-1;
                                vehicle.setName(name + "(" + validateId + ")");
                            }

                        }
                        vehicleList1.addVehicle(vehicle);
                        velocityLimitList= new VelocityLimitList();

                    }
                    if (qName.equalsIgnoreCase("gear_list")) {
//                        energy = new Energy(minRpmEnergy, maxRpmEnergy, finalDriveRatioEnergy);
//                        energy.setEnergy_regeneration_ratio(energy_regenerationRatio);

                        energy.setGearList(gearList);
                        vehicle.setEnergy(energy);
                    }
                    if (qName.equalsIgnoreCase("final_drive_ratio")) {
                        energy = new Energy(minRpmEnergy, maxRpmEnergy, finalDriveRatioEnergy);
//                        energy.setEnergy_regeneration_ratio(energy_regenerationRatio);

                    }
                    if (qName.equalsIgnoreCase("throttle")) {
                        energy.getThrottleLst().addThrottle(throttle);

                    }
                    if (qName.equalsIgnoreCase("regime")) {
                        Regime regime = new Regime(torqueEnergy, rpm_low, rpm_high, SFC);
                        SFC = 0;
                        throttle.getRegimes().add(regime);

                    }
                      if (qName.equalsIgnoreCase("energy_regeneration_ratio")) {
                       
                        energy.setEnergy_regeneration_ratio(energy_regenerationRatio);
//                          System.out.println("--------------ENTROU-------------------");
                      }

                }

                public void characters(char ch[], int start, int length)
                        throws SAXException {

                    if (typeTag) {
                        typeVehicle = new String(ch, start, length);
                        vehicle.setType(typeVehicle);

                        typeTag = false;
                    }
                    if (motorizationTag) {
                        motorizationVehicle = new String(ch, start, length);
                        vehicle.setMotorization(motorizationVehicle);

                        motorizationTag = false;
                    }
                    if (fuelTag) {
                        fuelVehicle = new String(ch, start, length);
                        vehicle.setFuel(fuelVehicle);

                        fuelTag = false;
                    }
                    if (massTag) {
                        String temp = new String(ch, start, length);
                        String splited[] = temp.split(" ");
                        massVehicle = Float.parseFloat(splited[0]);
                        vehicle.setMass(massVehicle);
                        massTag = false;
                    }
                    if (loadTag) {
                        String temp = new String(ch, start, length);
                        String splited[] = temp.split(" ");
                        loadVehicle = Float.parseFloat(splited[0]);
                        vehicle.setLoad(loadVehicle);

                        loadTag = false;
                    }
                    if (dragTag) {
                        dragVehicle = Float.parseFloat(new String(ch, start, length));
                        vehicle.setDrag(dragVehicle);
                        dragTag = false;
                    }
                    if (frontal_areaTag) {
                        frontal_area = Float.parseFloat(new String(ch, start, length));
                        vehicle.setFrontalArea(frontal_area);
                        frontal_areaTag = false;

                    }
                    if (rrcTag) {
                        rrcVehicle = Float.parseFloat(new String(ch, start, length));
                        rrcTag = false;
                        vehicle.setRrc(rrcVehicle);
                    }
                    if (wheelSizeTag) {
                        wheelSizeVehicle = Float.parseFloat(new String(ch, start, length));
                        vehicle.setWheelsize(wheelSizeVehicle);
                        wheelSizeTag = false;
                    }
                    if (segmentTypeTag) {
                        segmentTypeVelocity = new String(ch, start, length);
                        velocityLimity.setSegmentType(segmentTypeVelocity);
                        
                        segmentTypeTag = false;
                    }
                    if (limitTag) {
                        limitVelocity = (Float.parseFloat(new String(ch, start, length)) * 1000) / 3600;
                        velocityLimity.setLimit(limitVelocity);
                        limitTag = false;
                    }
                    if (velocityLimitTag) {
                        velocityLimitList.addVelocityLimit(velocityLimity);
                        velocityLimitTag = false;
                    }
                  
                    
                    if (torqueTag) {
                        torqueEnergy = Integer.parseInt(new String(ch, start, length));
                        torqueTag = false;
                    }
                    if (rpmTag) {
                        rpmEnergy = Float.parseFloat(new String(ch, start, length));
                        rpmTag = false;
                    }

                    if (minRpmTag) {
                        minRpmEnergy = Integer.parseInt(new String(ch, start, length));
                        //     System.out.println("min rpm :" + minRpmEnergy);
                        minRpmTag = false;
                    }
                    if (maxRpmTag) {
                        maxRpmEnergy = Integer.parseInt(new String(ch, start, length));
                        //    System.out.println("max rpm :" + maxRpmEnergy);
                        maxRpmTag = false;
                    }
                    if (finalDriveRatioTag) {
                        finalDriveRatioEnergy = Float.parseFloat(new String(ch, start, length));
                        //   System.out.println("final drive ratio :" + finalDriveRatioEnergy);
                        finalDriveRatioTag = false;
                    }
                    if (ratioTag) {
                        ratioEnergy = Float.parseFloat(new String(ch, start, length));
                        gear.setRatio(ratioEnergy);
                        //  System.out.println("ratio: " + ratioEnergy);
                        ratioTag = false;
                    }
                    if (gearTag) {
                        gearList.addGear(gear);
                        //  System.out.println("gear");
                        gearTag = false;
                    }
                    if (gearListTag) {
                        gearListTag = false;

                        //    System.out.println("gearList");
                    }
                    if (energyTag) {
                        energyTag = false;

                        //     System.out.println("energy");
                    }
                    if (vehicleTag) {
                        vehicleTag = false;
                    }
                    if (throttleTag) {
                        //     System.out.println("-->" + new String(ch, start, length) + "--");
                        throttleTag = false;
                    }
                    if (sfcTag) {
                        SFC = Float.parseFloat(new String(ch, start, length));
                        //    System.out.println("-->SFC :" + SFC);
                        sfcTag = false;
                    }
                    if (rpm_lowTag) {
                        rpm_low = Integer.parseInt(new String(ch, start, length));
                        //      System.out.println("Low --->>"+rpm_low);
                        rpm_lowTag = false;
                    }
                    if (rpm_highTag) {
                        rpm_high = Integer.parseInt(new String(ch, start, length));
                        //      System.out.println("-->>"+rpm_high);
                        rpm_highTag = false;
                    }

                    if (regimeTag) {
                        //       System.out.println("ID -->" + throttle.getId());
                        regimeTag = false;
                    }

                    if (energy_regeneration_ratioTag) {
                        energy_regenerationRatio = Float.parseFloat(new String(ch, start, length));
                        energy_regeneration_ratioTag = false;
                    }
                }
            };
            File file = new File(path);
            InputStream inputStream = new FileInputStream(file);
            Reader reader = new InputStreamReader(inputStream, "UTF-8");

            InputSource is = new InputSource(reader);
            is.setEncoding("UTF-8");

            saxParser.parse(is, handler);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
