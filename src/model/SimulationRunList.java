
package model;

import java.util.ArrayList;
import java.util.List;

public class SimulationRunList {

    private List<SimulationRun> simulationRuns;
    
    public SimulationRunList(){
        this.simulationRuns=new ArrayList<>();
    }
    
    public boolean removeSimulation(SimulationRun simulationRun){
                return this.getSimulationRuns().contains(simulationRun)
                ? false
                : this.getSimulationRuns().remove(simulationRun);
    }
    
    
    public boolean addSimulation(SimulationRun simulationRun) {
        return this.getSimulationRuns().contains(simulationRun)
                ? false
                : this.getSimulationRuns().add(simulationRun);
    }

    /**
     * @return the simulationRuns
     */
    public List<SimulationRun> getSimulationRuns() {
        return simulationRuns;
    }

    /**
     * @param simulationRuns the simulationRuns to set
     */
    public void setSimulationRuns(List<SimulationRun> simulationRuns) {
        this.simulationRuns = simulationRuns;
    }



}
