/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hichampt
 */
public class ExportDataFile {

    static final String tagsInicio = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\"http://www.w3.org/TR/html4/loose.dtd\">";
    static final String head = "<head>\n  <title> </title>\n  <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\n</head> ";
    static final String tagsInicioTITULOEN = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\"><title>Network Analisys Result</title>";
    static final String body = "<body>";
    static final String logo = "<img src=\"http://www.mediana.pt/imagens/noticias/712x297/isep_gr.jpg\" alt=\"Mountain View\" style=\"width:300px;height:90px;\" align=\"right\">";
    static final String espaco = "<br/><br/><br/><br/><br/><br/><br/><br/>";
    static final String h1 = "<h1 align=\"center\" >Results obtained from comparison of vehicle</h1>";
    static final String table = "<table border=\"1\"  align=\"center\" width=\"70%\">\n"
            + "	<tr align=\"center\" bgcolor=\"#cccccc\">\n"
            + "		<td width=\"30%\"><b>List of Sections </b></td>\n"
            + "		<td width=\"20%\"><b>Traveling Time </b></td>\n"
            + "		<td width=\"15%\"><b>Energy Consumption</b> </td>\n"
            + "		<td width=\"40%\"><b>Toll Costs</b>  </td>\n"
            + "	</tr>";

    static final String endTable = "</table>";
    static final String tagsFinalEN = "<br><br><br><br><br><br><br><br><br><div><p><p><p align=\"center\">Copyright © 2016 Todos os direitos reservados<b> G30 From 2DG</b>. </p></p><p align=\"center\">Date/hour:" + new Date() + "</p>\n</div></body></html>";

    private NetworkAnalisysResults networdAnaliseResults;
    private List<RoadSection> sections = new ArrayList<>();

    public String sections() {
        String sections = "";
        for (RoadSection rs : this.sections) {
            sections = sections + " " + rs.getRoadId() + " " + rs.getTypology() + "\n";
        }
        return sections;
    }

    public void criarHTML(File file) throws FileNotFoundException {

        String resultadosHTML = "";
        StringBuilder aux = new StringBuilder();
        String sections = sections();
      //  resultadosHTML += tagsInicio + tagsInicioTITULOEN + tagsCSSh1 + tagsCSSh2 + tagsCSSbody + tagsCSSh3;

        //Tratamento dos resultados
//        resultadosHTML += "<body>\n<div style = \"text-align:center;\">\n<h1>Experience</h1>\n</div>\n<div style = \"text-align:center;\">\n<pre style=\"color: #FFF3F3\">" + tagsExplicacaoEN + "</pre>\n</div>\n";
        resultadosHTML += "<h2 style = \"text-align:center;\">&nbsp;</h2>\n<h2 style = \"text-align:center;\">Results</h2>\n<div style = \"text-align:center;\"></div>\n";

        //dados
        resultadosHTML += "<h2 style = \"text-align:center;\">Materials and their Constants</h2>\n<div style = \"text-align:center;\">\n<pre style=\"color: #FFF3F3; text-align:center;\">" + sections;
        resultadosHTML += "<h3 style=\"text-align:center;\">traveling Time - " + this.networdAnaliseResults.getTravelingTime() + "</h3>";
        resultadosHTML += "<h3 style=\"text-align:center;\">Energy Consumption - " + this.networdAnaliseResults.getEnergyConsumption() + "</h3>";
        resultadosHTML += "<h3 style=\"text-align:center;\">Toll Costs - " + this.networdAnaliseResults.getTollCosts() + "</h3>";

        resultadosHTML += "";
        //resultadosHTML += "<h3 style=\"text-align:center;\">Fluxo Térmico - " + fluxoTermico + " W</h3>\n<h3 style=\"text-align:center;\">Resistência Térmica - " + resistencia + " m<sup>2</sup>K/W</h3>\n";
        resultadosHTML += "<h2>Conclusão&nbsp;</h2>\n<p>&nbsp;</p>\n<div style = \"text-align:center;\">\n<pre style=\"color: #FFF3F3\">";

        //Exportacao dos resultados para o ficheiro
        resultadosHTML += tagsFinalEN;
        aux.append(resultadosHTML);
        String nome = file.getAbsolutePath();
        Formatter out = new Formatter(nome);
        out.format("%s", aux.toString());
        out.close();

    }

    public void createHTMLCompareVehicle(File file, List<NetworkAnalisysResults> lst) throws FileNotFoundException {

        String resultadosHTML = "";
        StringBuilder aux = new StringBuilder();
        String sections = sections();
        resultadosHTML += tagsInicio + head + tagsInicioTITULOEN + body + logo + espaco + h1 + table;

        //dados
        for (NetworkAnalisysResults lst1 : lst) {
            resultadosHTML += "	<tr align=\"center\">\n"
                    + "		<td width=\"30%\"><b>" + lst1.getSections() + " </b></td>\n"
                    + "		<td width=\"20%\"><b> " + lst1.getTravelingTime() + " </b></td>\n"
                    + "		<td width=\"15%\"><b>" + lst1.getEnergyConsumption() + "</b> </td>\n"
                    + "		<td width=\"40%\"><b>" + lst1.getTollCosts() + "</b>  </td>\n"
                    + "	</tr>";
        }

        resultadosHTML += endTable;
        resultadosHTML += tagsFinalEN;
        aux.append(resultadosHTML);
        String nome = file.getAbsolutePath();
        Formatter out = new Formatter(nome);
        out.format("%s", aux.toString());
        out.close();

    }
}
