
package model;

import java.util.List;


public class Simulation {
    private int id;
    private String name;
    private String description;
    private TrafficList lstTraffic;
    private SimulationRunList simulationRuns;
    
    public Simulation(String name,String description){
        this.name=name;
        this.description=description;
        this.lstTraffic=new TrafficList();
        this.simulationRuns=new SimulationRunList();
    }
    public Simulation(int id,String name,String description){
        this.id=id;
        this.name=name;
        this.description=description;
        this.lstTraffic=new TrafficList();
        this.simulationRuns=new SimulationRunList();
    }

    /**
     * @return the simulationRuns
     */
    public SimulationRunList getSimulationRuns() {
        return simulationRuns;
    }

    /**
     * @param simulationRuns the simulationRuns to set
     */
    public void setSimulationRuns(SimulationRunList simulationRuns) {
        this.simulationRuns = simulationRuns;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the lstTraffic
     */
    public TrafficList getLstTraffic() {
        return lstTraffic;
    }

    /**
     * @param lstTraffic the lstTraffic to set
     */
    public void setLstTraffic(TrafficList lstTraffic) {
        this.lstTraffic = lstTraffic;
    }
    
    public Simulation clone(String name, String description) {
        Simulation sClone = new Simulation(name, description);
        sClone.setLstTraffic(lstTraffic);
        return sClone;
    }
    
    @Override
    public String toString(){
        return "Simulation - Name: "+name+"\nDescription: "+description+"\n"+lstTraffic
                +"\n"+simulationRuns;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

}
