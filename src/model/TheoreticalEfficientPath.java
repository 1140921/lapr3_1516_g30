package model;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class TheoreticalEfficientPath extends BestPath {

    @Override
    public NetworkAnalisysResults getPath(Graph<Junction, List<RoadSection>> g,
            Junction orig, Junction dest, Vehicle vehicle) {

        Deque<Junction> shortPath = new LinkedList<>();
        for (Edge<Junction, List<RoadSection>> e : g.edges()) {
            float menor = 0;
            float aux;
            for (RoadSection roads : e.getElement()) {
                aux = energy(vehicle, roads);
                if (menor == 0 || aux < menor) {
                    menor = aux;
                }
            }
            e.setWeight(menor);
        }

        GraphAlgorithms.shortestPath(g, orig, dest, shortPath);

        List<RoadSection> pathEdge = new ArrayList<>();
        Junction shortPathArray[] = new Junction[shortPath.size()];
        shortPathArray = shortPath.toArray(shortPathArray);
        double energyExpended = 0;
        int j = 0;
        float tolls = 0;
        float travelTime = 0;
        for (int i = 1; i < shortPathArray.length; i++) {
            Vertex<Junction, List<RoadSection>> vOrig = g.getVertex(shortPathArray[j]);
            Vertex<Junction, List<RoadSection>> vDest = g.getVertex(shortPathArray[i]);

            for (RoadSection roads : g.getEdge(vOrig, vDest).getElement()) {
                double weight = g.getEdge(vOrig, vDest).getWeight();
                if (energy(vehicle, roads) == weight) {
                    pathEdge.add(roads);
                    energyExpended = energyExpended + weight;
                    tolls = tolls + roads.getToll();
                    travelTime = travelTime + timeRoadSection(roads, vehicle);
                }
            }
            j++;
        }

        return new NetworkAnalisysResults(pathEdge, travelTime, (float) energyExpended, tolls);
    }

    public float energy(Vehicle v, RoadSection rs) {
        float energyExpended = 0;
        for (Segment s : rs.getSegmentList().getSegmentList()) {
            float velocity = velocity(v, rs.getTypology(), s);
            energyExpended = energyExpended + Calculus.work(v.getMass()+v.getLoad(),
                    velocity, v.getRrc(), rs.getWindSpeed(), rs.getWindDirection(), s.getSlope(), s.getLength());
        }

        return energyExpended;
    }

    @Override
    public String toString() {
        return "Theoretical most energy efficient path";
    }

}
