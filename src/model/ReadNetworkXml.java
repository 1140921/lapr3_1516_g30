/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import model.Junction;
import model.Project;
import model.RoadSection;
import model.Segment;
import model.SegmentList;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ReadNetworkXml implements InputData {

    private Project project;
    private RoadSection roadSection;
    private Segment segmentRoad;
    private SegmentList segmentList;
    private List<Junction> junctionList = new ArrayList<>();
    private Junction beginJunction;
    private Junction endJunction;

    @Override
    public void read(Project project, String path) {
        this.project = project;
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            DefaultHandler handler = new DefaultHandler() {
                boolean node = false;
                boolean road_section = false;
                boolean road = false;
                boolean typology = false;
                boolean direction = false;
                boolean toll = false;
                boolean wind_direction = false;
                boolean wind_speed = false;
                boolean projectId = false;
                boolean segment_list = false;
                boolean segment = false;
                boolean height = false;
                boolean slope = false;
                boolean Sglength = false;
                boolean max_velocity = false;
                boolean min_velocity = false;
                boolean number_vehicles = false;

                String roadSectionBegin = "";
                String roadSectionEnd = "";
                String roadID = "";
                String typologyRoad = "";
                String directionRoad = "";
                String tollRoad = "";
                String wind_directionRoad = "";
                String wind_speedRoad = "";
                String segmentId = "";
                String segementHeight = "";
                String segementSlope = "";
                String segmentLength = "";
                String segementRrc = "";
                String segmentMax_velocity = "";
                String segmentMin_velocity = "";
                String segementNumber_vehicles = "";
                float floatHeigth;
                float floatSlope;
                float floatLength;
                float intMaxVelocity;
                float intMinVelocity;
                int intNumberVehicles;
                Direction enumDirection;
                float floatTollRoad;
                float floatWindDirection;
                float floatWindSpeed;

                public void startElement(String uri, String localName,
                        String qName, Attributes attributes)
                        throws SAXException {

                    if (qName.equalsIgnoreCase("network")) {
//                        String id = attributes.getValue("id");
//
//                        String description = attributes.getValue("description");
//                        project.setName(id);
//                        project.setDescription(description);
                        projectId = true;
                    }

                    if (qName.equalsIgnoreCase("node")) {
                        node = true;
                        String nodeId = attributes.getValue("id");
                        Junction node = new Junction(nodeId);
                        project.addJunction(node);
//                        junctionList.add(node);

                    }
                    if (qName.equalsIgnoreCase("road_section")) {
//                        System.out.println("///////////////////////////");
//                        System.out.println("Road Section begin :" + attributes.getValue("begin"));
//                        System.out.println("Road Section end :" + attributes.getValue("end"));
//                        System.out.println("///////////////////////////");
                        roadSectionBegin = attributes.getValue("begin");
                        roadSectionEnd = attributes.getValue("end");

//                        for (Junction j : junctionList) {
//                            if (j.getId().equals(roadSectionBegin)) {
//                                beginJunction = j;
//                            }
//                            if (j.getId().equals(roadSectionEnd)) {
//                                endJunction = j;
//                            }
//
//                        }
                        for (Vertex<Junction, List<RoadSection>> junctionLst : project.getRoadNetwork().vertices()) {
                            junctionLst.getElement().getId();
                            if (junctionLst.getElement().getId().equals(roadSectionBegin)) {
                                beginJunction = junctionLst.getElement();
                            }
                            if (junctionLst.getElement().getId().equals(roadSectionEnd)) {
                                endJunction = junctionLst.getElement();
                            }
                        }

                        road_section = true;
                    }

                    if (qName.equalsIgnoreCase("road")) {

                        road = true;
                    }
                    if (qName.equalsIgnoreCase("typology")) {
                        typology = true;
                    }
                    if (qName.equalsIgnoreCase("direction")) {
                        direction = true;
                    }
                    if (qName.equalsIgnoreCase("toll")) {
                        toll = true;
                    }
                    if (qName.equalsIgnoreCase("wind_direction")) {
                        wind_direction = true;
                    }
                    if (qName.equalsIgnoreCase("wind_speed")) {
                        wind_speed = true;
                    }
                    if (qName.equalsIgnoreCase("segment_list")) {
                        segmentList = new SegmentList();

                        segment_list = true;
                    }
                    if (qName.equalsIgnoreCase("segment")) {

                        segmentId = attributes.getValue("id");
//                        System.out.println("----------" + segmentId);
                        segment = true;
                    }
                    if (qName.equalsIgnoreCase("height")) {

                        height = true;
                    }
                    if (qName.equalsIgnoreCase("slope")) {

                        slope = true;
                    }
                    if (qName.equalsIgnoreCase("length")) {

                        Sglength = true;
                    }

                    if (qName.equalsIgnoreCase("max_velocity")) {

                        max_velocity = true;
                    }
                    if (qName.equalsIgnoreCase("min_velocity")) {

                        min_velocity = true;
                    }
                    if (qName.equalsIgnoreCase("number_vehicles")) {

                        number_vehicles = true;
                    }

                }

                public void endElement(String uri, String localName,
                        String qName) throws SAXException {

                    if (qName.equalsIgnoreCase("wind_speed")) {
                        roadSection = new RoadSection(roadID, beginJunction, endJunction, typologyRoad, enumDirection, floatTollRoad, floatWindDirection, floatWindSpeed);
//                        System.out.println("------------------------------------------------>");
                    }

                    if (qName.equalsIgnoreCase("segment")) {
                        segmentRoad = new Segment(segmentId, floatHeigth, floatSlope, floatLength, intNumberVehicles);
//                              System.out.println("->"+intMaxVelocity);
                        segmentRoad.setMaxVelocity(intMaxVelocity);
                        segmentRoad.setMinVelocity(intMinVelocity);
                        roadSection.getSegmentList().addSegment(segmentRoad);
                    }
                    if (qName.equalsIgnoreCase("segment_list")) {
                        project.addSection(roadSection);
//                        System.out.println("RoadSection -->:" + roadSection.getRoadId());
//                        System.out.println("<---------------->" + "beginJunction" + beginJunction + "\nendJunction" + endJunction);
                    }

                }

                public void characters(char ch[], int start, int length)
                        throws SAXException {

                    if (node) {
                        node = false;
                    }

                    if (road) {
                        roadID = new String(ch, start, length);

//                        System.out.println("road : " + new String(ch, start, length));
                        road = false;
                    }
                    if (typology) {
//                        System.out.println("typology :" + new String(ch, start, length));
                        typologyRoad = new String(ch, start, length);
                        typology = false;
                    }
                    if (direction) {
//                        System.out.println("direction :" + new String(ch, start, length));

                        directionRoad = new String(ch, start, length);
                        if (directionRoad.equalsIgnoreCase("DIRECT")) {
                            enumDirection = Direction.DIRECT;
                        } else if (directionRoad.equalsIgnoreCase("REVERSE")) {
                            enumDirection = Direction.REVERSE;
                        } else {
                            enumDirection = Direction.BIDIRECTIONAL;
                        }
                        direction = false;
                    }
                    if (toll) {
//                        System.out.println("toll :" + new String(ch, start, length));
                        tollRoad = new String(ch, start, length);
                        floatTollRoad = Float.parseFloat(tollRoad);
                        toll = false;
                    }
                    if (wind_direction) {
//                        System.out.println("wind_direction :" + new String(ch, start, length));
                        wind_directionRoad = new String(ch, start, length);
                        floatWindDirection = Float.parseFloat(wind_directionRoad);
                        wind_direction = false;
                    }
                    if (wind_speed) {
//                        System.out.println("wind_speed :" + new String(ch, start, length));
                        wind_speedRoad = new String(ch, start, length);
                        String windSpeed[] = wind_speedRoad.split(" ");
                        floatWindSpeed = Float.parseFloat(windSpeed[0]);
                        wind_speed = false;

                    }

                    if (height) {
//                        System.out.println("height :" + new String(ch, start, length));
                        segementHeight = new String(ch, start, length);
                        floatHeigth = Float.parseFloat(segementHeight);
                        height = false;

                    }
                    if (slope) {
//                        System.out.println("slope :" + new String(ch, start, length));
                        segementSlope = new String(ch, start, length);
                        String[] slopePer = segementSlope.split("%");
                        floatSlope = Float.parseFloat(slopePer[0].replace(",", "."));
                        slope = false;

                    }
                    if (Sglength) {
//                        System.out.println("Sglength :" + new String(ch, start, length));
                        segmentLength = new String(ch, start, length);
                        String lengthKm[] = segmentLength.split(" ");
                        if (lengthKm[1].equalsIgnoreCase("km")) {
                            floatLength = Float.parseFloat(lengthKm[0]) * 1000;
                        } else {
                            floatLength = Float.parseFloat(lengthKm[0]);
                        }

                        Sglength = false;

                    }

                    if (max_velocity) {
//                        System.out.println("max_velocity :" + new String(ch, start, length));
                        segmentMax_velocity = new String(ch, start, length);
                        String maxVelocityKm[] = segmentMax_velocity.split(" ");
                        if (maxVelocityKm[1].equalsIgnoreCase("Km/h") && segmentMax_velocity != null) {
                            intMaxVelocity = Float.parseFloat(maxVelocityKm[0]) * 1000 / 3600;
                        }

                        max_velocity = false;

                    }
                    if (min_velocity) {
//                        System.out.println("min_velocity :" + new String(ch, start, length));
                        segmentMin_velocity = new String(ch, start, length);
                        String minVelocityKm[] = segmentMin_velocity.split(" ");
                        if (minVelocityKm[1].equalsIgnoreCase("Km/h") && segmentMin_velocity != null) {
                            intMinVelocity = Float.parseFloat(minVelocityKm[0]) * 1000 / 3600;
                        }

                        min_velocity = false;

                    }
                    if (number_vehicles) {
//                        System.out.println("max_velocity :" + new String(ch, start, length));
                        segementNumber_vehicles = new String(ch, start, length);
                        intNumberVehicles = Integer.parseInt(segementNumber_vehicles);
                        number_vehicles = false;

                    }
                    if (segment) {
//                        System.out.println("--->" + segementHeight);
//                        System.out.println("->" + floatHeigth);
//                        segmentRoad = new Segment(segmentId, floatHeigth, floatSlope, floatLength, intNumberVehicles);
//
//                        segmentRoad.setMaxVelocity(intMaxVelocity);
//                        segmentRoad.setMinVelocity(intMinVelocity);
//                        roadSection.getSegmentList().addSegment(segmentRoad);
//                        segmentList.addSegment(segmentRoad);
                        segment = false;

                    }
                    if (segment_list) {
//                        System.out.println("segment_list :" + new String(ch, start, length));
//                        System.out.println("----------");

                        segmentList = new SegmentList();
                        segment_list = false;
                    }
                    if (road_section) {

//                        roadSection = new RoadSection(roadID, beginJunction, endJunction, typologyRoad, enumDirection, floatTollRoad, floatWindDirection, floatWindSpeed);
//                        project.getRoadNetwork().insertEdge(beginJunction, endJunction, roadSection, 0);
//                        System.out.println("\n<--->"+ project.getRoadNetwork().insertEdge(beginJunction, endJunction, roadSection, 0));
                        road_section = false;
                    }
                }

            };
            File file = new File(path);
            InputStream inputStream = new FileInputStream(file);
            Reader reader = new InputStreamReader(inputStream, "UTF-8");

            InputSource is = new InputSource(reader);
            is.setEncoding("UTF-8");

            saxParser.parse(is, handler);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
