/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;

/**
 *
 * @author 1140921
 */
public class SimulationResults {

    private Project activeProject;
    private BestPath bestPath;
    private HashMap<TrafficPattern, Float> averageEnergyConsumption;

    public SimulationResults(ProjectRegistry projectRegistry) {
        this.averageEnergyConsumption = new HashMap<>();
        setActiveProject(projectRegistry.getActiveProject());
    }

    public HashMap<TrafficPattern, Float> averageEnergyConsumptionForEachTraffic(TrafficList trafficPatternList) {
        for (TrafficPattern trafficPattern : trafficPatternList.getLstTrafficPattern()) {
            Float energyConsumption = bestPath.getPath(getActiveProject().getRoadNetwork(), trafficPattern.getBeg(), trafficPattern.getEnd(), trafficPattern.getVehicle()).getEnergyConsumption();
            averageEnergyConsumption.put(trafficPattern, energyConsumption);

        }
        return averageEnergyConsumption;
    }
    
    public HashMap<TrafficPattern, Float> averageEnergyConsumptionForEachTrafficInAllSgements(TrafficList trafficPatternList) {
        for (TrafficPattern trafficPattern : trafficPatternList.getLstTrafficPattern()) {
            Float energyConsumption = bestPath.getPath(getActiveProject().getRoadNetwork(), trafficPattern.getBeg(), trafficPattern.getEnd(), trafficPattern.getVehicle()).getEnergyConsumption();
            averageEnergyConsumption.put(trafficPattern, energyConsumption);

        }
        return averageEnergyConsumption;
    }

    /**
     * @return the bestPath
     */
    public BestPath getBestPath() {
        return bestPath;
    }

    /**
     * @param bestPath the bestPath to set
     */
    public void setBestPath(BestPath bestPath) {
        this.bestPath = bestPath;
    }

    /**
     * @return the activeProject
     */
    public Project getActiveProject() {
        return activeProject;
    }

    /**
     * @param activeProject the activeProject to set
     */
    public void setActiveProject(Project activeProject) {

        this.activeProject = activeProject;
    }

}
