
package model;

import java.util.List;

public abstract class BestPath {
    
public abstract NetworkAnalisysResults getPath(Graph<Junction, List<RoadSection>> g, Junction orig, Junction dest, Vehicle vehicle);
    
    protected static float velocity(Vehicle vehicle, String type, Segment seg) {
        Energy eAux=vehicle.getEnergy();
        float velocity = Calculus.carVelocity(eAux.getMaxRpm(),eAux.getFinalDriveRatio(),
                eAux.getGearList().last().getRatio());
              
        for (VelocityLimit vl : vehicle.getVelocityLimitList().getVelocityLimitList()) {
            if (type.equalsIgnoreCase(vl.getSegmentType()) && velocity>vl.getLimit()) {
                velocity= vl.getLimit();
            }
        }
        if (seg.getMaxVelocity() < velocity) {
            velocity = seg.getMaxVelocity();
        }
        return velocity;
    }

      protected static float timeRoadSection(RoadSection rs, Vehicle vehicle) {
        float time = 0;
        float velocity;
        float timeAux;
        for (Segment s : rs.getSegmentList().getSegmentList()) {
            velocity = velocity(vehicle, rs.getTypology(), s);
            timeAux = s.getLength() / velocity;
            time = time + timeAux;
        }
        return time;
    }



}
