
package model;

import java.util.ArrayList;
import java.util.List;



public class VelocityLimitList {

    private List<VelocityLimit> velocityLimitList;
    
    public VelocityLimitList(){
        this.velocityLimitList=new ArrayList<>();
    }

    /**
     * @return the velocityLimitList
     */
    public List<VelocityLimit> getVelocityLimitList() {
        return velocityLimitList;
    }

    /**
     * @param velocityLimitList the velocityLimitList to set
     */
    public void setVelocityLimitList(List<VelocityLimit> velocityLimitList) {
        this.velocityLimitList = velocityLimitList;
    }
    
    public boolean addVelocityLimit(VelocityLimit vl){
        return this.velocityLimitList.add(vl);
    }
    
   @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Velocity Limit List: \n");
        for (VelocityLimit v : velocityLimitList) {
            sb.append(v);
            sb.append("\n");
        }
        return sb.toString();
    }
}
