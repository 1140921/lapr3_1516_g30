
package model;

import java.util.ArrayList;
import java.util.List;


public class Throttle {
    private String id;
    private List<Regime> regimes;
    
    public Throttle(String id){
        this.id=id;
        this.regimes=new ArrayList<>();
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the regimes
     */
    public List<Regime> getRegimes() {
        return regimes;
    }
    
    public Regime getRegime(int rpm){
        for(Regime r:regimes){
            if(r.getRpmLow()<=rpm && rpm <=r.getRpmHigh()){
                return r;
            }
        }
    return null;
    }
    @Override
    
    public String toString(){
       return "Throttle id: "+id+"\nRegimes: "+regimes; 
    }
}
