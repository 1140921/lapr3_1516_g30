
package model;


public class TrafficPattern {
    
    private Junction beg;
    private Junction end;
    private Vehicle vehicle;
    private float arrivalRate;
    private String arrivalRateUnit;
    
    
    public TrafficPattern(Junction beg,Junction end,Vehicle vehicle,float arrivalRate){
        this.beg=beg;
        this.end=end;
        this.vehicle=vehicle;
        this.arrivalRate=arrivalRate;
    }

    /**
     * @return the beg
     */
    public Junction getBeg() {
        return beg;
    }

    /**
     * @param beg the beg to set
     */
    public void setBeg(Junction beg) {
        this.beg = beg;
    }

    /**
     * @return the end
     */
    public Junction getEnd() {
        return end;
    }

    /**
     * @param end the end to set
     */
    public void setEnd(Junction end) {
        this.end = end;
    }

    /**
     * @return the vehicle
     */
    public Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * @param vehicle the vehicle to set
     */
    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    /**
     * @return the arrivalRate
     */
    public float getArrivalRate() {
        return arrivalRate;
    }

    /**
     * @param arrivalRate the arrivalRate to set
     */
    public void setArrivalRate(float arrivalRate) {
        this.arrivalRate = arrivalRate;
    }
    
    @Override
    public String toString(){
        return "Traffic - Beg: "+this.beg.getId()+" End: "+this.getEnd()+" Vehicle: "+vehicle.getName()
                +" Arrival Rate: "+arrivalRate;
    }

    /**
     * @return the arrivalRateUnit
     */
    public String getArrivalRateUnit() {
        return arrivalRateUnit;
    }

    /**
     * @param arrivalRateUnit the arrivalRateUnit to set
     */
    public void setArrivalRateUnit(String arrivalRateUnit) {
        this.arrivalRateUnit = arrivalRateUnit;
    }
}
