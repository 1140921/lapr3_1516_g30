
package model;


public class Segment {
    
    private String id;
    private float initialHeight;
    private float slope;
    private float length;
    private float maxVelocity;
    private float minVelocity;
    private int maxNumberVehicles;
    
    
    public Segment(String id,float initialHeight,float slope,float length,int maxNumberVehicles){
     this.id=id;
     this.initialHeight=initialHeight;
     this.length=length;
     this.maxNumberVehicles=maxNumberVehicles;
     this.slope=slope;
    }
    
    public Segment(String id,float initialHeight,float slope,float length,float maxVelocity,
            float minVelocity,int maxNumberVehicles){
     this.id=id;
     this.initialHeight=initialHeight;
     this.length=length;
     this.maxNumberVehicles=maxNumberVehicles;
     this.slope=slope;
     this.minVelocity=minVelocity;
     this.maxVelocity=maxVelocity;
    }
    
    /**
     * @return the index
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the index to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the initialHeight
     */
    public float getInitialHeight() {
        return initialHeight;
    }

    /**
     * @param initialHeight the initialHeight to set
     */
    public void setInitialHeight(float initialHeight) {
        this.initialHeight = initialHeight;
    }

    /**
     * @return the slope
     */
    public float getSlope() {
        return slope;
    }

    /**
     * @param slope the slope to set
     */
    public void setSlope(float slope) {
        this.slope = slope;
    }

    /**
     * @return the length
     */
    public float getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(float length) {
        this.length = length;
    }

    /**
     * @return the maxVelocity
     */
    public float getMaxVelocity() {
        return maxVelocity;
    }

    /**
     * @param maxVelocity the maxVelocity to set
     */
    public void setMaxVelocity(float maxVelocity) {
        this.maxVelocity = maxVelocity;
    }

    /**
     * @return the minVelocity
     */
    public float getMinVelocity() {
        return minVelocity;
    }

    /**
     * @param minVelocity the minVelocity to set
     */
    public void setMinVelocity(float minVelocity) {
        this.minVelocity = minVelocity;
    }

    /**
     * @return the maxNumberVehicles
     */
    public int getMaxNumberVehicles() {
        return maxNumberVehicles;
    }

    /**
     * @param maxNumberVehicles the maxNumberVehicles to set
     */
    public void setMaxNumberVehicles(int maxNumberVehicles) {
        this.maxNumberVehicles = maxNumberVehicles;
    }
    
    public String toString(){
     return "Segment id: "+id+"\nHeight: "+initialHeight+"\nSlope: "+slope+"\nLength: "
             +length+"\nMaxVelocity: "+maxVelocity+"\nMin Velocity: "+minVelocity
             +"\nMax Number of Vehicles: "+maxNumberVehicles;
    }
}
