
package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class SegmentList {
   private ArrayList<Segment>segmentList;

     public SegmentList() {
        segmentList = new ArrayList<>();
    }


    public List<Segment> getSegmentList() {
        return new ArrayList(segmentList);
    }

    public void setSegmentList(List<Segment> segmentList) {
        this.segmentList.clear();
        this.segmentList.addAll(segmentList);
    }

    public Segment getSegment(int indice) {
        return this.segmentList.get(indice);
    }

    public boolean addSegment(Segment segment) {
        return this.segmentList.contains(segment)
                ? false
                : this.segmentList.add(segment);
    }


    public boolean removeSegment(Segment segment) {
        return this.segmentList.remove(segment);
    }

    public Segment removeSegment(int indice) {
        return this.segmentList.remove(indice);
    }

    public boolean removeAll(SegmentList segmentList) {
        return this.segmentList.removeAll(segmentList.getSegmentList());
    }

    public void clean() {
        this.segmentList.clear();
    }

    public int size() {
        return this.segmentList.size();
    }

    public boolean isEmpty() {
        return this.segmentList.isEmpty();
    }

    public Segment[] getArray() {
        return this.segmentList.toArray(
                new Segment[this.segmentList.size()]);
    }
    
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }
        SegmentList otherSegmentList = (SegmentList) outroObjeto;
        return this.segmentList.equals(otherSegmentList.getSegmentList());
    }
    
   @Override
    public String toString(){
        StringBuilder sb=new StringBuilder();
        sb.append("Segment List: \n");
        for(Segment s :segmentList){
            sb.append(s);
            sb.append("\n");
        }
    return sb.toString();
    }
}
