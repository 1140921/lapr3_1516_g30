package model;

public class Vehicle {
    
    private int id;
    private String name;
    private String description;
    private String type;
    private String motorization;    
    private String fuel;
    private float mass;    
    private float load;    
    private float drag;
    private float frontalArea;
    private float rrc;
    private float wheelsize;
    private Energy energy;
    private VelocityLimitList velocityLimitList;    
    
    public Vehicle(){        
    this.velocityLimitList=new VelocityLimitList();
    }
    
    public Vehicle(String name,String description,String type,String motorization,String fuel,float mass,float load,float drag,float frontalArea,float rrc,float wheelsize){
        this.name=name;
        this.description=description;
        this.type=type;
        this.motorization=motorization;
        this.fuel=fuel;
        this.mass=mass;
        this.load=load;
        this.drag=drag;
        this.frontalArea=frontalArea;
        this.rrc=rrc;
        this.wheelsize=wheelsize;
        this.velocityLimitList=new VelocityLimitList();
    }
    
        public Vehicle(int id,String name,String description,String type,String motorization,String fuel,float mass,float load,float drag,float frontalArea,float rrc,float wheelsize){
        this.name=name;
        this.description=description;
        this.type=type;
        this.motorization=motorization;
        this.fuel=fuel;
        this.mass=mass;
        this.load=load;
        this.drag=drag;
        this.frontalArea=frontalArea;
        this.rrc=rrc;
        this.wheelsize=wheelsize;
        this.id=id;
        this.velocityLimitList=new VelocityLimitList();
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the motorization
     */
    public String getMotorization() {
        return motorization;
    }

    /**
     * @param motorization the motorization to set
     */
    public void setMotorization(String motorization) {
        this.motorization = motorization;
    }

    /**
     * @return the mass
     */
    public float getMass() {
        return mass;
    }

    /**
     * @param mass the mass to set
     */
    public void setMass(float mass) {
        this.mass = mass;
    }

    /**
     * @return the load
     */
    public float getLoad() {
        return load;
    }

    /**
     * @param load the load to set
     */
    public void setLoad(float load) {
        this.load = load;
    }

    /**
     * @return the drag
     */
    public float getDrag() {
        return drag;
    }

    /**
     * @param drag the dragCoefficient to set
     */
    public void setDrag(float drag) {
        this.drag = drag;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the fuel
     */
    public String getFuel() {
        return fuel;
    }

    /**
     * @param fuel the fuel to set
     */
    public void setFuel(String fuel) {
        this.fuel = fuel;
    }


    /**
     * @return the rrc
     */
    public float getRrc() {
        return rrc;
    }

    /**
     * @param rrc the rrc to set
     */
    public void setRrc(float rrc) {
        this.rrc = rrc;
    }

    /**
     * @return the wheelsize
     */
    public float getWheelsize() {
        return wheelsize;
    }

    /**
     * @param wheelsize the wheelsize to set
     */
    public void setWheelsize(float wheelsize) {
        this.wheelsize = wheelsize;
    }

    /**
     * @return the energy
     */
    public Energy getEnergy() {
        return energy;
    }

    /**
     * @return the velocityLimitList
     */
    public VelocityLimitList getVelocityLimitList() {
        return velocityLimitList;
    }

    /**
     * @param energy the energy to set
     */
    public void setEnergy(Energy energy) {
        this.energy = energy;
    }

    /**
     * @param velocityLimitList the velocityLimitList to set
     */
    public void setVelocityLimitList(VelocityLimitList velocityLimitList) {
        this.velocityLimitList = velocityLimitList;
    }

    /**
     * @return the frontalArea
     */
    public float getFrontalArea() {
        return frontalArea;
    }

    /**
     * @param frontalArea the frontalArea to set
     */
    public void setFrontalArea(float frontalArea) {
        this.frontalArea = frontalArea;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        Vehicle v = (Vehicle) o;

        return this.name.equals(v.getName()) && this.type.equals(v.getType()) &&
                this.motorization.equals(v.getMotorization()) && this.mass==v.getMass();
    }
    
    
        @Override
    public String toString(){
        return "Vehicle: "+name;//+"\nDescription: "+description+"\nType: "+type
                //+"\nMotorization: "+motorization+"\nFuel: "+fuel+"\nMass: "+mass+"\nLoad: "+load
                //+"\nDrag: "+drag+"\nFrontal Area: "+frontalArea+"\nRRC: "+rrc+"\nWheel Size: "+wheelsize
                //+"\n"+velocityLimitList+energy;
    }
    
    public String allInformationSring(){
                return "Vehicle: \nName: "+name+"\nDescription: "+description+"\nType: "+type
                +"\nMotorization: "+motorization+"\nFuel: "+fuel+"\nMass: "+mass+"\nLoad: "+load
                +"\nDrag: "+drag+"\nFrontal Area: "+frontalArea+"\nRRC: "+rrc+"\nWheel Size: "+wheelsize
                +"\n"+velocityLimitList+energy;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }


}
