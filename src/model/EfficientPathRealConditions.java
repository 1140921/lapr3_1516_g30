package model;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class EfficientPathRealConditions extends BestPath {

    @Override
    public NetworkAnalisysResults getPath(Graph<Junction, List<RoadSection>> g, Junction orig, Junction dest, Vehicle vehicle) {
        Deque<Junction> shortPath = new LinkedList<>();
        for (Edge<Junction, List<RoadSection>> e : g.edges()) {
            float menor = 0;
            float aux;
            for (RoadSection roads : e.getElement()) {
                aux = energy(vehicle, roads);
                if (menor == 0 || aux < menor) {
                    menor = aux;
                }
            }
            e.setWeight(menor);
        }

        GraphAlgorithms.shortestPath(g, orig, dest, shortPath);

        List<RoadSection> pathEdge = new ArrayList<>();
        Junction shortPathArray[] = new Junction[shortPath.size()];
        shortPathArray = shortPath.toArray(shortPathArray);
        double fuelConsumed = 0;
        int j = 0;
        float tolls = 0;
        for (int i = 1; i < shortPathArray.length; i++) {
            Vertex<Junction, List<RoadSection>> vOrig = g.getVertex(shortPathArray[j]);
            Vertex<Junction, List<RoadSection>> vDest = g.getVertex(shortPathArray[i]);

            for (RoadSection roads : g.getEdge(vOrig, vDest).getElement()) {
                double weight = g.getEdge(vOrig, vDest).getWeight();
                if (energy(vehicle, roads) == weight) {
                    pathEdge.add(roads);
                    fuelConsumed = fuelConsumed + weight;
                    tolls = tolls + roads.getToll();
                }
            }
            j++;
        }

        float travelTime = (float) time(pathEdge, vehicle);

        return new NetworkAnalisysResults(pathEdge, travelTime, (float) fuelConsumed, tolls);
    }

    public static float energy(Vehicle vehicle, RoadSection rs) {
        float totalFuelConsumed = 0;
        for (Segment s : rs.getSegmentList().getSegmentList()) {
            float velocity = velocity(vehicle, rs.getTypology(), s);
            Gear currentGear = vehicle.getEnergy().getGearList().last();
            Throttle currentThrottle = vehicle.getEnergy().getThrottleLst().getTrottle(25);
            System.out.println("velocity: " + velocity);
            int rpm = Calculus.rpm(velocity, vehicle.getEnergy().getFinalDriveRatio(),
                    currentGear.getRatio(), vehicle.getWheelsize() / 2);
            int torque = currentThrottle.getRegime(rpm).getTorque();
            float appliedForce = getAppliedForce(rs, s, vehicle, torque, currentGear, velocity);
            System.out.println("road: " + rs.getRoadId() + " beg node: " + rs.getBeginningNode().getId() + "end node: " + rs.getEndingNode().getId() + " seg: " + s.getId());
            System.out.println("rpm: " + rpm);
            System.out.println("torque: " + torque);
            System.out.println("applied force: " + appliedForce);
            while (appliedForce < 0) {
                Throttle tAux = vehicle.getEnergy().getThrottleLst().getNextThrottle(currentThrottle);
                Gear gAux = vehicle.getEnergy().getGearList().getReduction(currentGear);

                //condicao verifica se e possivel acelerar, se acelerador ja estive a 100 %, reduz-se velocidade
                if (tAux == null) {
                    currentGear = gAux;
                    System.out.println("reduz");
                } else {
                    System.out.println("acelera");
                    currentThrottle = tAux;
                }
                rpm = Calculus.rpm(velocity, vehicle.getEnergy().getFinalDriveRatio(),
                        currentGear.getRatio(), vehicle.getWheelsize() / 2);
                torque = currentThrottle.getRegime(rpm).getTorque();
                appliedForce = getAppliedForce(rs, s, vehicle, torque, currentGear, velocity);
                System.out.println("rpm: " + rpm);
                System.out.println("torque: " + torque);
                System.out.println("appliedForce: " + appliedForce);
                System.out.println("---------------------------------------------------------");
            }

            float fuelInThisSegment = energyInThisSegment(s, velocity, torque, rpm, currentThrottle, vehicle);
            
            System.out.println("energy: "+fuelInThisSegment);
            System.out.println("-------------------------------------------------------------");
            totalFuelConsumed = totalFuelConsumed + fuelInThisSegment;
        }

        return totalFuelConsumed;
    }

    private static float energyInThisSegment(Segment s, float velocity, int torque, int rpm, Throttle t, Vehicle v) {
        float power = (float)Calculus.powerGeneratedByEngine(torque, rpm);
        float fuelInThisSegment = 0;
        float time = s.getLength() / velocity;
        if (v.getMotorization().equalsIgnoreCase("combustion")) {
            float SFC = t.getRegime(rpm).getSFC();
            fuelInThisSegment = (float) Calculus.instantFuelConsumption(v.getFuel(), SFC, power, time);
        } else if (v.getMotorization().equalsIgnoreCase("electric")) {
            fuelInThisSegment=(power*time);
            if(s.getSlope()<0){
                fuelInThisSegment=(power*time)-(power*time*v.getEnergy().getEnergy_regeneration_ratio());
            }
        }

        return fuelInThisSegment;
    }

    private static float getAppliedForce(RoadSection rs, Segment s, Vehicle v, int torque, Gear currentGear, float speed) {

        float f;
        if (s.getSlope() == 0) {
            f = Calculus.ForceAppliedToTheVehicleFlatSurface(torque, v.getEnergy().getFinalDriveRatio(),
                    currentGear.getRatio(), v.getWheelsize() / 2, v.getRrc(), v.getMass() + v.getLoad(), v.getFrontalArea(),
                    v.getDrag(),
                    rs.getWindSpeed(), rs.getWindDirection(), speed);
        } else {
            f = Calculus.ForceAppliedToTheVehicle(torque, v.getEnergy().getFinalDriveRatio(), currentGear.getRatio(),
                    v.getWheelsize() / 2, v.getRrc(), v.getMass() + v.getLoad(), v.getFrontalArea(), v.getDrag(), s.getSlope(),
                    rs.getWindSpeed(), rs.getWindDirection(), speed);
        }

        return f;
    }

    private static float time(List<RoadSection> listRoads, Vehicle vehicle) {
        float time = 0;
        float velocity;
        float timeAux;
        for (RoadSection rs : listRoads) {
            for (Segment s : rs.getSegmentList().getSegmentList()) {
                velocity = velocity(vehicle, rs.getTypology(), s);
                timeAux = s.getLength() / velocity;
                time = time + timeAux;
            }
        }
        return time;
    }

    @Override
    public String toString() {
        return "Most efficient path in real conditions";
    }
}
