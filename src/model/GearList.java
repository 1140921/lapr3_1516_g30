
package model;

import java.util.ArrayList;
import java.util.List;


public class GearList {
private List<Gear> gearList;

public GearList(){
    this.gearList=new ArrayList<>();
}

    /**
     * @return the gearList
     */
    public List<Gear> getGearList() {
        return gearList;
    }

    public boolean addGear(Gear g){
        if(!validaId(g.getId())){
            return false;
        }
        return this.gearList.add(g);
    }
    private boolean validaId(String id){
        for(Gear g:gearList){
            if(id.equals(g.getId())){
                return false;
            }
        }
        return true;
    }

    /**
     * @param gearList the gearList to set
     */
    public void setGearList(List<Gear> gearList) {
        this.gearList = gearList;
    }
    
    public Gear first(){
        return this.gearList.get(0);
    }
    public Gear last(){
        return this.gearList.get(this.gearList.size()-1);
    }
        
    public Gear getReduction(Gear gear){
        int index= gearList.indexOf(gear)-1;
        if(index<0){
            return null;
        }
        return this.getGearList().get(index);
    }
    
    
    @Override
    public String toString(){
      StringBuilder sb = new StringBuilder();
      sb.append("Gear List:");
      sb.append("\n");
      for(Gear g:gearList){
          sb.append(g.toString());
          sb.append("\n");
      }  
    return sb.toString();
    }
    
}
