 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ReadSimulationXml implements InputData {

    private String line;
    private Junction beginJunction;
    private Junction endJunction;
    private Vehicle vehicle;
    private Simulation simulation;

    @Override
    public void read(Project activeProject, String xmlFile) {
        this.line = xmlFile;
        simulation =activeProject.getSimulations().getActiveSimulation();
        try {

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            DefaultHandler handler = new DefaultHandler() {

                boolean traffic_list = false;
                boolean traffic_pattern = false;
                boolean vehicle_simulation = false;
                boolean arrival_rate = false;

                float arrivel;
                String arrivalUnit;

                public void startElement(String uri, String localName,
                        String qName, Attributes attributes)
                        throws SAXException {

                    if (qName.equalsIgnoreCase("Simulation")) {
//                        String id = attributes.getValue("id");
//
//                        String description = attributes.getValue("description");
//                        simulation.setName(id);
//                        simulation.setDescription(description);
                    }

                    if (qName.equalsIgnoreCase("traffic_list")) {

                        traffic_list = true;
                    }
                    if (qName.equalsIgnoreCase("traffic_pattern")) {
                        traffic_pattern = true;
//                        try {
                            String begin = attributes.getValue("begin");
                            beginJunction = activeProject.getJunctionById(begin);
                            if (beginJunction == null) {
                                throw new NullPointerException("begin junction not available!");
                            }
                            String end = attributes.getValue("end");
                            endJunction = activeProject.getJunctionById(end);
                            if (endJunction == null) {
                                throw new NullPointerException("end junction not available!");

                            }
//                        } catch (NullPointerException e) {
//                            JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
//                        }

                    }

                    if (qName.equalsIgnoreCase("vehicle")) {
                        vehicle_simulation = true;
                    }
                    if (qName.equalsIgnoreCase("arrival_rate")) {
                        arrival_rate = true;
                    }

                }

                public void endElement(String uri, String localName,
                        String qName) throws SAXException {

                    if (qName.equalsIgnoreCase("traffic_pattern")) {
                        TrafficPattern traffic = new TrafficPattern(beginJunction, endJunction, vehicle, arrivel);
                        traffic.setArrivalRateUnit(arrivalUnit);
                        simulation.getLstTraffic().addTrafficPattern(traffic);
                        activeProject.getSimulations().addSimulation(simulation);
                    }

                }

                public void characters(char ch[], int start, int length)
                        throws SAXException {

                    if (traffic_list) {
                        traffic_list = false;
                    }

                    if (vehicle_simulation) {
                        String vehicle_name = new String(ch, start, length);
                        vehicle = activeProject.getVehicleList().getVehicleByName(vehicle_name);
                        vehicle_simulation = false;
                    }
                    if (arrival_rate) {
                        String arrivelV[] = new String(ch, start, length).split(" ");
                        arrivel = Float.parseFloat(arrivelV[0]);
                        arrivalUnit=arrivelV[1];
                        arrival_rate = false;
                    }
                }

            };
            File file = new File(xmlFile);
            InputStream inputStream = new FileInputStream(file);
            Reader reader = new InputStreamReader(inputStream, "UTF-8");

            InputSource is = new InputSource(reader);
            is.setEncoding("UTF-8");

            saxParser.parse(is, handler);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
