/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.awt.Desktop;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 1140921
 */
public abstract class CsvDataWritter implements ExportData{

    //Delimiter used in CSV file

    private static final String COMMA_DELIMITER = ";";
    private static final String NEW_LINE_SEPARATOR = "\n";
    private Project project;

    //CSV file header
    private static final String FILE_HEADER = "Project,Simulation,Simulation Period,Time Step ,Execution Time,Number of Records Stored ";

    public void writeFile(String path, ProjectRegistry projectRgistry) {
        project = projectRgistry.getActiveProject();
        //Create new students objects
        SimulationResults results1 = new SimulationResults(null);

        FileWriter fileWriter = null;

        try {
            fileWriter = new FileWriter(path);

            //Write the CSV file header
            fileWriter.append(FILE_HEADER.toString());

            //Add a new line separator after the header
            fileWriter.append(NEW_LINE_SEPARATOR);
            fileWriter.append((project.getName()));

            //Write a new student object list to the CSV file
            for (Simulation results : project.getSimulations().getSimulations()) {

                fileWriter.append(COMMA_DELIMITER);

                fileWriter.append(COMMA_DELIMITER);

                fileWriter.append(COMMA_DELIMITER);

                fileWriter.append(COMMA_DELIMITER);

                fileWriter.append(NEW_LINE_SEPARATOR);
            }

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {

            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
            }

        }
    }
}
