package model;

import java.util.ArrayList;
import java.util.List;

public class Project {
    private int id;
    private String name;
    private String description;
    private Graph<Junction, List<RoadSection>> roadNetwork;
    private VehiclesList vehicleList;
    private SimulationList simulations;

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
        roadNetwork = new Graph<>(true);
        vehicleList = new VehiclesList();
        this.simulations = new SimulationList();
    }
        public Project(int id,String name, String description) {
        this.name = name;
        this.description = description;
        this.id=id;
        roadNetwork = new Graph<>(true);
        vehicleList = new VehiclesList();
        this.simulations = new SimulationList();
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public void setRoadNetwork(Graph<Junction, List<RoadSection>> roadNetwork) {
        this.roadNetwork = roadNetwork;
    }

    /**
     * @return the roadNetwork
     */
    public Graph<Junction, List<RoadSection>> getRoadNetwork() {
        return roadNetwork;
    }

    public boolean addJunction(Junction node) {
        if (roadNetwork.getVertex(node) == null) {
            roadNetwork.insertVertex(node);
              return true;
        } else {
            return false;
        }

      
    }

    public boolean addSection(RoadSection s) {

        Vertex<Junction, List<RoadSection>> vOrig = roadNetwork.getVertex(s.getBeginningNode());
        Vertex<Junction, List<RoadSection>> vDest = roadNetwork.getVertex(s.getEndingNode());

        if (vOrig == null || vDest == null) {
            return false;
        }
        List<RoadSection> ls = new ArrayList<>();
        ls.add(s);
        List<RoadSection> ls1 = new ArrayList<>();

        if (s.getDirection().equals(Direction.DIRECT)) {
            if (roadNetwork.getEdge(vOrig, vDest) == null) {
                roadNetwork.insertEdge(s.getBeginningNode(), s.getEndingNode(), ls, 0);
            } else {
                roadNetwork.getEdge(vOrig, vDest).getElement().add(s);
            }
        } else {
            if (roadNetwork.getEdge(vOrig, vDest) == null) {
                roadNetwork.insertEdge(s.getBeginningNode(), s.getEndingNode(), ls, 0);
            } else {
                roadNetwork.getEdge(vOrig, vDest).getElement().add(s);
            }
            RoadSection s1 = new RoadSection(s.getRoadId(), s.getEndingNode(), s.getBeginningNode(),
                    s.getTypology(), s.getDirection(), s.getToll(), Calculus.reverseAngle(s.getWindDirection()), s.getWindSpeed());
            s1.setSegmentList(s.getSegmentList());
            ls1.add(s1);
            if (roadNetwork.getEdge(vDest, vOrig) == null) {
                roadNetwork.insertEdge(s.getEndingNode(), s.getBeginningNode(), ls1, 0);
            } else {
                roadNetwork.getEdge(vDest, vOrig).getElement().add(s1);
            }
        }
        return true;
    }

    //Project p=projeto que se quer clonar
    //nome e desricao para criar o novo projeto
    public Project clone(String nome, String descricao) {
        Graph<Junction, List<RoadSection>> roadNetworkClone = this.getRoadNetwork().clone();
        Project projectClone = new Project(nome, descricao);
        projectClone.setRoadNetwork(roadNetworkClone);
        projectClone.setVehicleList(vehicleList);
        return projectClone;
    }

    /**
     * @return the vehicleList
     */
    public VehiclesList getVehicleList() {
        return vehicleList;
    }

    public String toString() {
        return "Project: \nName: " + name + "\nDescription: " + description;
    }

    /**
     * @return the simulations
     */
    public SimulationList getSimulations() {
        return simulations;
    }

    public Junction getJunctionById(String id) {
        for (Vertex<Junction, List<RoadSection>> j : roadNetwork.vertices()) {
            if (j.getElement().getId().equals(id)) {
                return j.getElement();
            }
        }
        return null;
    }

    /**
     * @param vehicleList the vehicleList to set
     */
    public void setVehicleList(VehiclesList vehicleList) {
        this.vehicleList = vehicleList;
    }

    /**
     * @param simulations the simulations to set
     */
    public void setSimulations(SimulationList simulations) {
        this.simulations = simulations;
    }

    public List getJunctions() {
        List<String> junctionLst = new ArrayList<>();
        for (Vertex<Junction, List<RoadSection>> j : roadNetwork.vertices()) {
            junctionLst.add(j.getElement().getId());
            return junctionLst;
        }
        return null;
    }
    
    public List<RoadSection> getSections(){
        
        List<RoadSection> sections=new ArrayList<>();
        
        for(Edge<Junction,List<RoadSection>> e:this.roadNetwork.edges()){
            for(RoadSection rs:e.getElement()){
                sections.add(rs);
            }
        }
        return sections;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    
    
}
