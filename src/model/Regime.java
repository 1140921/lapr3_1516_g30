
package model;


public class Regime {

    private int torque;
    private int rpmLow;
    private int rpmHigh;
    private float SFC;
    
    
    public Regime(int torque,int rpmLow,int rpmHigh,float SFC){
        this.torque=torque;
        this.rpmLow=rpmLow;
        this.rpmHigh=rpmHigh;
        this.SFC=SFC;
    }

    /**
     * @return the torque
     */
    public int getTorque() {
        return torque;
    }

    /**
     * @param torque the torque to set
     */
    public void setTorque(int torque) {
        this.torque = torque;
    }

    /**
     * @return the rpmLow
     */
    public int getRpmLow() {
        return rpmLow;
    }

    /**
     * @param rpmLow the rpmLow to set
     */
    public void setRpmLow(int rpmLow) {
        this.rpmLow = rpmLow;
    }

    /**
     * @return the rpmHigh
     */
    public int getRpmHigh() {
        return rpmHigh;
    }

    /**
     * @param rpmHigh the rpmHigh to set
     */
    public void setRpmHigh(int rpmHigh) {
        this.rpmHigh = rpmHigh;
    }

    /**
     * @return the SFC
     */
    public float getSFC() {
        return SFC;
    }

    /**
     * @param SFC the SFC to set
     */
    public void setSFC(float SFC) {
        this.SFC = SFC;
    }
    
    @Override
    public String toString(){
        return "Regime - Torque: "+torque+" RPM Low: "+rpmLow+" RPM High: "+rpmHigh+" SFC: "+SFC;
    }


}
