
package model;


public class VelocityLimit {

    private String segmentType;
    private float limit;
    
    public VelocityLimit(){
        
    }
    
    public VelocityLimit(String segmentType,float limit){
        this.segmentType=segmentType;
        this.limit=limit;
    }

    /**
     * @return the segmentType
     */
    public String getSegmentType() {
        return segmentType;
    }

    /**
     * @param segmentType the segmentType to set
     */
    public void setSegmentType(String segmentType) {
        this.segmentType = segmentType;
    }

    /**
     * @return the limit
     */
    public float getLimit() {
        return limit;
    }

    /**
     * @param limit the limit to set
     */
    public void setLimit(float limit) {
        this.limit = limit;
    }
    @Override
    public String toString(){
      return "Velocity Limit: \nSegment Type: "+segmentType+"\nLimit: "+limit;   
    }
}
