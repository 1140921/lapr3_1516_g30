package model;

import java.util.ArrayList;
import java.util.List;

public class ThrottleList {

    private List<Throttle> throttleLst;

    public ThrottleList() {
        this.throttleLst = new ArrayList<>();
    }

    /**
     * @return the throttleLst
     */
    public List<Throttle> getThrottleLst() {
        return throttleLst;
    }

    /**
     * @param ThrottleLst the throttleLst to set
     */
    public void setThrottleLst(List<Throttle> ThrottleLst) {
        this.throttleLst = ThrottleLst;
    }

    public Throttle getTrottle(int percentage) {
        for (Throttle t : throttleLst) {
            if (t.getId().equals(Integer.toString(percentage))) {
                return t;
            }
        }
        return null;
    }

    public boolean addThrottle(Throttle throttle) {
        return this.throttleLst.contains(throttle)
                ? false
                : this.throttleLst.add(throttle);
    }

    public boolean removeThrottle(Throttle throttle) {
        return this.throttleLst.remove(throttle);
    }

    public Throttle first() {
        return this.throttleLst.get(0);
    }

    public Throttle last() {
        return this.throttleLst.get(this.throttleLst.size() - 1);
    }

    public Throttle getNextThrottle(Throttle t) {
        int indexNext= throttleLst.indexOf(t)+1;
        if(indexNext>throttleLst.indexOf(last())){
            return null;
        }
        return throttleLst.get(indexNext);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Throttle List: \n");
        for (Throttle t : throttleLst) {
            sb.append(t);
            sb.append("\n");
        }
        return sb.toString();
    }
}
