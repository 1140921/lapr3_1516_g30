package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VehiclesList {

    private ArrayList<Vehicle> vehicleList;

    public VehiclesList() {
        vehicleList = new ArrayList<>();
    }

    public List<Vehicle> getVehicleList() {
        return new ArrayList(vehicleList);
    }

    public void setVehicleList(List<Vehicle> vehicleList) {
        this.vehicleList.clear();
        this.vehicleList.addAll(vehicleList);
    }

    public Vehicle getVehicle(int indice) {
        return this.vehicleList.get(indice);
    }

    public boolean addVehicle(Vehicle vehicle) {
        return this.vehicleList.contains(vehicle)
                ? false
                : this.vehicleList.add(vehicle);
    }

    public boolean removeVehicle(Vehicle vehicle) {
        return this.vehicleList.remove(vehicle);
    }

    public Vehicle removeVehicle(int indice) {
        return this.vehicleList.remove(indice);
    }

    public boolean removeAll(VehiclesList vehicleList) {
        return this.vehicleList.removeAll(vehicleList.getVehicleList());
    }

    public void clean() {
        this.vehicleList.clear();
    }

    public int size() {
        return this.vehicleList.size();
    }

    public boolean isEmpty() {
        return this.vehicleList.isEmpty();
    }

    public Vehicle[] getArray() {
        return this.vehicleList.toArray(
                new Vehicle[this.vehicleList.size()]);
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }
        VehiclesList otherVehicleList = (VehiclesList) outroObjeto;
        return this.vehicleList.equals(otherVehicleList.getVehicleList());
    }
    public Vehicle getVehicleByName(String name){
        for(Vehicle v: vehicleList){
            if(v.getName().equals(name)){
                return v;
            }
        }
        return null;
    }
    
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Vehicle List: \n");
        for (Vehicle v : vehicleList) {
            sb.append(v);
            sb.append("\n");
        }
        return sb.toString();
    }

}
