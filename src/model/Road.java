
package model;

import java.util.ArrayList;


public class Road {
  private String id;
  private ArrayList<RoadSection> listRoads;
  
  public Road(String id){
      this.id=id;
      listRoads=new ArrayList<>();
  }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the listRoads
     */
    public ArrayList<RoadSection> getListRoads() {
        return listRoads;
    }

    /**
     * @param listRoads the listRoads to set
     */
    public void setListRoads(ArrayList<RoadSection> listRoads) {
        this.listRoads = listRoads;
    }
  

}
