package model;

import java.util.ArrayList;
import java.util.List;

public class SimulationList {

    private List<Simulation> simulations;
    private Simulation activeSimulation;

    public SimulationList() {
        this.simulations = new ArrayList<>();
    }

    /**
     * @return the simulations
     */
    public List<Simulation> getSimulations() {
        return simulations;
    }

    /**
     * @param simulations the simulations to set
     */
    public void setSimulations(List<Simulation> simulations) {
        this.simulations = simulations;
    }

    /**
     * @return the activeSimulation
     */
    public Simulation getActiveSimulation() {
        return activeSimulation;
    }

    /**
     * @param activeSimulation the activeSimulation to set
     */
    public void setActiveSimulation(Simulation activeSimulation) {
        this.activeSimulation = activeSimulation;
    }

    public boolean addSimulation(Simulation simulation) {
        return this.simulations.contains(simulation)
                ? false
                : this.simulations.add(simulation);
    }

    public Simulation newSimulation(String name, String description) {
        return new Simulation(name, description);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Simulation List: \n");
        for (Simulation s : simulations) {
            sb.append(s);
            sb.append("\n");
        }
        return sb.toString();
    }
}
